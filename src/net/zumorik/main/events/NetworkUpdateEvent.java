package net.zumorik.main.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public final class NetworkUpdateEvent extends Event {

	private static final HandlerList handlers = new HandlerList();

	public NetworkUpdateEvent() {

	}

	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}
}
