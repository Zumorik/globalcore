package net.zumorik.main.mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.bukkit.entity.Player;

import net.zumorik.main.Core;

public class MiscDatabase {

	private PreparedStatement preparedStmt;
	private Core core;
	private String host;
	private String password;
	private String username;
	private String db;
	private Connection connection;

	public MiscDatabase(Core core) {
		this.core = core;
		host = core.getConfig().getString("host");
		password = core.getConfig().getString("settings_password");
		username = core.getConfig().getString("settings_user");
		db = core.getConfig().getString("settings");
		try {
			connection = getConnection("jdbc:mariadb://" + host + "/" + db, username, password);
			this.preparedStmt = connection.prepareStatement(
					"CREATE TABLE IF NOT EXISTS PlayerData (playerName varchar(36) PRIMARY KEY NOT NULL,Name varchar(20), normal INT, rare INT, legendary INT, player INT, speed INT, flight INT)");
			this.preparedStmt.executeUpdate();
			this.preparedStmt.close();
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public Connection getCurrentConnection() throws ClassNotFoundException, SQLException {
		if (connection != null) {
			if (connection.isValid(100)) {
				return connection;
			}else
			connection = getConnection("jdbc:mariadb://" + host + "/" + db, username, password);
			return connection;
		}else
		connection = getConnection("jdbc:mariadb://" + host + "/" + db, username, password);
		return connection;
	}

	public void close() throws SQLException {
		connection.close();
		System.out.println("[ACN - Database connection closed forcibly]");
	}

	public static Connection getConnection(String dbURL, String user, String password)
			throws SQLException, ClassNotFoundException {
		Class.forName("org.mariadb.jdbc.Driver");
		return DriverManager.getConnection(dbURL, user, password);
	}

	public void addNormalMysteryBox(Player p, int amount) {
		String UPDATE_QUERY = "UPDATE PlayerData SET normal=? WHERE playerName=?";
		int amounts = getNormalMysteryBox(p) + amount;
		try (PreparedStatement preparedStatement = getCurrentConnection().prepareStatement(UPDATE_QUERY)) {
			preparedStatement.setInt(1, amounts);
			preparedStatement.setString(2, p.getUniqueId().toString());
			preparedStatement.executeUpdate();
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void setPlayer(Player p, int setting) {
		String UPDATE_QUERY = "UPDATE PlayerData SET player=? WHERE playerName=?";
		try (PreparedStatement preparedStatement = getCurrentConnection().prepareStatement(UPDATE_QUERY)) {
			preparedStatement.setInt(1, setting);
			preparedStatement.setString(2, p.getUniqueId().toString());
			preparedStatement.executeUpdate();
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void setSpeed(Player p, int setting) {

		String UPDATE_QUERY = "UPDATE PlayerData SET speed=? WHERE playerName=?";
		try (PreparedStatement preparedStatement = getCurrentConnection().prepareStatement(UPDATE_QUERY)) {
			preparedStatement.setInt(1, setting);
			preparedStatement.setString(2, p.getUniqueId().toString());
			preparedStatement.executeUpdate();
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void setFlight(Player p, int setting) {
		String UPDATE_QUERY = "UPDATE PlayerData SET flight=? WHERE playerName=?";
		try (PreparedStatement preparedStatement = getCurrentConnection().prepareStatement(UPDATE_QUERY)) {
			preparedStatement.setInt(1, setting);
			preparedStatement.setString(2, p.getUniqueId().toString());
			preparedStatement.executeUpdate();
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public int getNormalMysteryBox(Player p) {
		String query = "SELECT normal FROM PlayerData WHERE playerName=?";
		try (PreparedStatement posted = getCurrentConnection().prepareStatement(query)) {
			posted.setString(1, p.getUniqueId().toString());
			ResultSet resultSet = posted.executeQuery();
			while (resultSet.next()) {
				return resultSet.getInt("normal");
			}
			return 0;
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public int getPlayer(Player p) {
		String query = "SELECT player FROM PlayerData WHERE playerName=?";
		try (PreparedStatement posted = getCurrentConnection().prepareStatement(query)) {
			posted.setString(1, p.getUniqueId().toString());
			ResultSet resultSet = posted.executeQuery();
			while (resultSet.next()) {
				return resultSet.getInt("player");
			}
			return 0;
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public int getFlight(Player p) {
		String query = "SELECT flight FROM PlayerData WHERE playerName=?";
		try (PreparedStatement posted = getCurrentConnection().prepareStatement(query)) {
			posted.setString(1, p.getUniqueId().toString());
			ResultSet resultSet = posted.executeQuery();
			while (resultSet.next()) {
				return resultSet.getInt("flight");
			}
			return 0;
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public int getSpeed(Player p) {
		String query = "SELECT speed FROM PlayerData WHERE playerName=?";
		try (PreparedStatement posted = getCurrentConnection().prepareStatement(query)) {
			posted.setString(1, p.getUniqueId().toString());
			ResultSet resultSet = posted.executeQuery();
			while (resultSet.next()) {
				return resultSet.getInt("speed");
			}
			return 0;
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public void removeNormalMysteryBox(Player p, int amount) {
		String UPDATE_QUERY = "UPDATE PlayerData SET normal=? WHERE playerName=?";
		int amounts = getNormalMysteryBox(p) - amount;
		try (PreparedStatement preparedStatement = getCurrentConnection().prepareStatement(UPDATE_QUERY)) {
			preparedStatement.setInt(1, amounts);
			preparedStatement.setString(2, p.getUniqueId().toString());
			preparedStatement.executeUpdate();
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void addRareMysteryBox(Player p, int amount) {
		String UPDATE_QUERY = "UPDATE PlayerData SET rare=? WHERE playerName=?";
		int amounts = getRareMysteryBox(p) + amount;
		try (PreparedStatement preparedStatement = getCurrentConnection().prepareStatement(UPDATE_QUERY)) {
			preparedStatement.setInt(1, amounts);
			preparedStatement.setString(2, p.getUniqueId().toString());
			preparedStatement.executeUpdate();
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public int getRareMysteryBox(Player p) {
		String query = "SELECT rare FROM PlayerData WHERE playerName=?";
		try (PreparedStatement posted = getCurrentConnection().prepareStatement(query)) {
			posted.setString(1, p.getUniqueId().toString());
			ResultSet resultSet = posted.executeQuery();
			while (resultSet.next()) {
				return resultSet.getInt("rare");
			}
			return 0;
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public void removeRareMysteryBox(Player p, int amount) {
		String UPDATE_QUERY = "UPDATE PlayerData SET rare=? WHERE playerName=?";
		int amounts = getRareMysteryBox(p) - amount;
		try (PreparedStatement preparedStatement = getCurrentConnection().prepareStatement(UPDATE_QUERY)) {
			preparedStatement.setInt(1, amounts);
			preparedStatement.setString(2, p.getUniqueId().toString());
			preparedStatement.executeUpdate();
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void addLegMysteryBox(Player p, int amount) {
		String UPDATE_QUERY = "UPDATE PlayerData SET legendary=? WHERE playerName=?";
		int amounts = getLegMysteryBox(p) + amount;
		try (PreparedStatement preparedStatement = getCurrentConnection().prepareStatement(UPDATE_QUERY)) {
			preparedStatement.setInt(1, amounts);
			preparedStatement.setString(2, p.getUniqueId().toString());
			preparedStatement.executeUpdate();
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public int getLegMysteryBox(Player p) {
		String query = "SELECT legendary FROM PlayerData WHERE playerName=?";
		try (PreparedStatement posted = getCurrentConnection().prepareStatement(query)) {
			posted.setString(1, p.getUniqueId().toString());
			ResultSet resultSet = posted.executeQuery();
			while (resultSet.next()) {
				return resultSet.getInt("legendary");
			}
			return 0;
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public void removeLegMysteryBox(Player p, int amount) {
		String UPDATE_QUERY = "UPDATE PlayerData SET legendary=? WHERE playerName=?";
		int amounts = getLegMysteryBox(p) - amount;
		try (PreparedStatement preparedStatement = getCurrentConnection().prepareStatement(UPDATE_QUERY)) {
			preparedStatement.setInt(1, amounts);
			preparedStatement.setString(2, p.getUniqueId().toString());
			preparedStatement.executeUpdate();
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void setName(Player p, int id) {
		String UPDATE_QUERY = "UPDATE PlayerData SET Name=? WHERE playerName=?";
		try (PreparedStatement preparedStatement = getCurrentConnection().prepareStatement(UPDATE_QUERY)) {
			preparedStatement.setString(1, p.getName());
			preparedStatement.setString(2, p.getUniqueId().toString());
			preparedStatement.executeUpdate();
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public String getName(Player p) {
		String query = "SELECT Name FROM PlayerData WHERE playerName=?";
		try (PreparedStatement posted = getCurrentConnection().prepareStatement(query)) {
			posted.setString(1, p.getUniqueId().toString());
			ResultSet resultSet = posted.executeQuery();
			while (resultSet.next()) {
				return resultSet.getString("Name");
			}
			return null;
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	public void createStatements(Player p) {
		if (getName(p) != null) {
			core.getMysteryCache().cacheUser(p);
			if (getName(p) == null) {
				setName(p, 0);
			}
		} else {
			try {
				connection = getCurrentConnection();
				String query = "INSERT INTO PlayerData (playerName, Name, normal, rare, legendary,player,speed,flight) VALUES (?,?,?,?,?,?,?,?)";
				PreparedStatement st;
				st = connection.prepareStatement(query);
				st.setString(1, p.getUniqueId().toString());
				st.setString(2, p.getName());
				st.setInt(3, 1);
				st.setInt(4, 1);
				st.setInt(5, 1);
				st.setInt(6, 1);
				st.setInt(7, 0);
				st.setInt(8, 0);
				st.executeUpdate();
				st.close();
			} catch (SQLException | ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
	}
}
