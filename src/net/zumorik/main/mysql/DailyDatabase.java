package net.zumorik.main.mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.bukkit.entity.Player;

import net.zumorik.main.Core;

public class DailyDatabase {

    private PreparedStatement preparedStmt;
    private Core core;
    private String host;
    private String password;
    private String username;
    private String db;
    private Connection connection;

    public DailyDatabase(Core core) {
        this.core = core;
        host = core.getConfig().getString("host");
        password = core.getConfig().getString("daily_password");
        username = core.getConfig().getString("daily_user");
        db = core.getConfig().getString("daily_db");
        try {
            connection = getConnection("jdbc:mariadb://" + host + "/" + db, username, password);
            this.preparedStmt = connection.prepareStatement(
                    "CREATE TABLE IF NOT EXISTS PlayerData (playerName varchar(36) PRIMARY KEY NOT NULL,Name varchar(20), monthly_user varchar(20), monthly_epic varchar(20) , monthly_vip varchar(20), monthly_elite varchar(20), monthly_ultra varchar(20), first_time INT, daily varchar(20), second_time INT)");
            this.preparedStmt.executeUpdate();
            this.preparedStmt.close();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public Connection getCurrentConnection() throws ClassNotFoundException, SQLException {
        if (connection != null) {
            if (connection.isValid(100)) {
                return connection;
            }else
                connection = getConnection("jdbc:mariadb://" + host + "/" + db, username, password);
            return connection;
        }else
            connection = getConnection("jdbc:mariadb://" + host + "/" + db, username, password);
        return connection;
    }

    public void close() throws SQLException {
        connection.close();
        System.out.println("[ACN - Database connection closed forcibly]");
    }

    public static Connection getConnection(String dbURL, String user, String password)
            throws SQLException, ClassNotFoundException {
        Class.forName("org.mariadb.jdbc.Driver");
        return DriverManager.getConnection(dbURL, user, password);
    }

    public void setPlayer(Player p, int setting) {
        String UPDATE_QUERY = "UPDATE PlayerData SET player=? WHERE playerName=?";
        try (PreparedStatement preparedStatement = getCurrentConnection().prepareStatement(UPDATE_QUERY)) {
            preparedStatement.setInt(1, setting);
            preparedStatement.setString(2, p.getUniqueId().toString());
            preparedStatement.executeUpdate();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void setMonthlyUser(Player p, String format) {
        String UPDATE_QUERY = "UPDATE PlayerData SET monthly_user=? WHERE playerName=?";
        try (PreparedStatement preparedStatement = getCurrentConnection().prepareStatement(UPDATE_QUERY)) {
            preparedStatement.setString(1, format);
            preparedStatement.setString(2, p.getUniqueId().toString());
            preparedStatement.executeUpdate();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void setMonthlyVIP(Player p, String format) {
        String UPDATE_QUERY = "UPDATE PlayerData SET monthly_vip=? WHERE playerName=?";
        try (PreparedStatement preparedStatement = getCurrentConnection().prepareStatement(UPDATE_QUERY)) {
            preparedStatement.setString(1, format);
            preparedStatement.setString(2, p.getUniqueId().toString());
            preparedStatement.executeUpdate();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void setMonthlyEpic(Player p, String format) {
        String UPDATE_QUERY = "UPDATE PlayerData SET monthly_epic=? WHERE playerName=?";
        try (PreparedStatement preparedStatement = getCurrentConnection().prepareStatement(UPDATE_QUERY)) {
            preparedStatement.setString(1, format);
            preparedStatement.setString(2, p.getUniqueId().toString());
            preparedStatement.executeUpdate();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void setMonthlyElite(Player p, String format) {
        String UPDATE_QUERY = "UPDATE PlayerData SET monthly_elite=? WHERE playerName=?";
        try (PreparedStatement preparedStatement = getCurrentConnection().prepareStatement(UPDATE_QUERY)) {
            preparedStatement.setString(1, format);
            preparedStatement.setString(2, p.getUniqueId().toString());
            preparedStatement.executeUpdate();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void setMonthlyUltra(Player p, String format) {
        String UPDATE_QUERY = "UPDATE PlayerData SET monthly_ultra=? WHERE playerName=?";
        try (PreparedStatement preparedStatement = getCurrentConnection().prepareStatement(UPDATE_QUERY)) {
            preparedStatement.setString(1, format);
            preparedStatement.setString(2, p.getUniqueId().toString());
            preparedStatement.executeUpdate();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void setFirstTime(Player p, int val) {
        String UPDATE_QUERY = "UPDATE PlayerData SET first_time=? WHERE playerName=?";
        try (PreparedStatement preparedStatement = getCurrentConnection().prepareStatement(UPDATE_QUERY)) {
            preparedStatement.setInt(1, val);
            preparedStatement.setString(2, p.getUniqueId().toString());
            preparedStatement.executeUpdate();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void setDaily(Player p, String format) {
        String UPDATE_QUERY = "UPDATE PlayerData SET daily=? WHERE playerName=?";
        try (PreparedStatement preparedStatement = getCurrentConnection().prepareStatement(UPDATE_QUERY)) {
            preparedStatement.setString(1, format);
            preparedStatement.setString(2, p.getUniqueId().toString());
            preparedStatement.executeUpdate();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void setSecondTime(Player p, int val) {
        String UPDATE_QUERY = "UPDATE PlayerData SET second_time=? WHERE playerName=?";
        try (PreparedStatement preparedStatement = getCurrentConnection().prepareStatement(UPDATE_QUERY)) {
            preparedStatement.setInt(1, val);
            preparedStatement.setString(2, p.getUniqueId().toString());
            preparedStatement.executeUpdate();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }


    public int getPlayer(Player p) {
        String query = "SELECT player FROM PlayerData WHERE playerName=?";
        try (PreparedStatement posted = getCurrentConnection().prepareStatement(query)) {
            posted.setString(1, p.getUniqueId().toString());
            ResultSet resultSet = posted.executeQuery();
            while (resultSet.next()) {
                return resultSet.getInt("player");
            }
            return 0;
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public int getFirstTime(Player p) {
        String query = "SELECT first_time FROM PlayerData WHERE playerName=?";
        try (PreparedStatement posted = getCurrentConnection().prepareStatement(query)) {
            posted.setString(1, p.getUniqueId().toString());
            ResultSet resultSet = posted.executeQuery();
            while (resultSet.next()) {
                return resultSet.getInt("first_time");
            }
            return 1;
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return 1;
    }

    public int getSecondTime(Player p) {
        String query = "SELECT second_time FROM PlayerData WHERE playerName=?";
        try (PreparedStatement posted = getCurrentConnection().prepareStatement(query)) {
            posted.setString(1, p.getUniqueId().toString());
            ResultSet resultSet = posted.executeQuery();
            while (resultSet.next()) {
                return resultSet.getInt("second_time");
            }
            return 1;
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return 1;
    }

    public void setName(Player p, int id) {
        String UPDATE_QUERY = "UPDATE PlayerData SET Name=? WHERE playerName=?";
        try (PreparedStatement preparedStatement = getCurrentConnection().prepareStatement(UPDATE_QUERY)) {
            preparedStatement.setString(1, p.getName());
            preparedStatement.setString(2, p.getUniqueId().toString());
            preparedStatement.executeUpdate();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public String getName(Player p) {
        String query = "SELECT Name FROM PlayerData WHERE playerName=?";
        try (PreparedStatement posted = getCurrentConnection().prepareStatement(query)) {
            posted.setString(1, p.getUniqueId().toString());
            ResultSet resultSet = posted.executeQuery();
            while (resultSet.next()) {
                return resultSet.getString("Name");
            }
            return null;
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getMonthly(Player p, String val) {
        String query = "SELECT " + val + " FROM PlayerData WHERE playerName=?";
        try (PreparedStatement posted = getCurrentConnection().prepareStatement(query)) {
            posted.setString(1, p.getUniqueId().toString());
            ResultSet resultSet = posted.executeQuery();
            while (resultSet.next()) {
                return resultSet.getString(val);
            }
            return "NA";
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return "NA";
    }

    public void createStatements(Player p) {
        if (getName(p) != null) {
            core.getDailyDataCache().cacheUser(p);
            if (getName(p) == null) {
                setName(p, 0);
            }
        } else {
            try {
                connection = getCurrentConnection();
                String query = "INSERT INTO PlayerData (playerName, name, monthly_user, monthly_vip, monthly_epic, monthly_elite, monthly_ultra, first_time, daily, second_time) VALUES (?,?,?,?,?,?,?,?,?,?)";
                PreparedStatement st;
                st = connection.prepareStatement(query);
                st.setString(1, p.getUniqueId().toString());
                st.setString(2, p.getName());
                st.setString(3, "NA");
                st.setString(4, "NA");
                st.setString(5, "NA");
                st.setString(6, "NA");
                st.setString(7, "NA");
                st.setInt(8, 0);
                st.setString(9, "NA");
                st.setInt(10, 0);
                st.executeUpdate();
                st.close();
            } catch (SQLException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}
