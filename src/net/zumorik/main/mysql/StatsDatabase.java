package net.zumorik.main.mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import net.zumorik.main.Core;

public class StatsDatabase {

	private Core core;

	private final String host;
	private final String password;
	private final String username;
	private final String db;
	private Connection connection;

	public StatsDatabase(Core core) {
		this.core = core;
		host = core.getConfig().getString("host");
		password = core.getConfig().getString("stats_password");
		username = core.getConfig().getString("stats_user");
		db = core.getConfig().getString("stats");
		try {
			connection = getConnection("jdbc:mariadb://" + host + "/" + db, username, password);
			PreparedStatement preparedStmt = connection.prepareStatement(
					"CREATE TABLE IF NOT EXISTS PlayerData (playerName varchar(36) PRIMARY KEY NOT NULL,Name varchar(20), Coins FLOAT,SWKills FLOAT,SWDeaths FLOAT,Level FLOAT,SWWins INT, SWKits varchar(1000) , SelectedKit varchar(500), CAGE INT, Language INT, ban INT, bantime varchar(50), reason varchar(50))");
			preparedStmt.executeUpdate();
			preparedStmt.close();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}

	public Connection getCurrentConnection() throws ClassNotFoundException, SQLException {
		if (connection != null) {
			if (connection.isValid(100)) {
				return connection;
			}else
			connection = getConnection("jdbc:mariadb://" + host + "/" + db, username, password);
			return connection;
		}else
		connection = getConnection("jdbc:mariadb://" + host + "/" + db, username, password);
		return connection;
	}

	public void close() throws SQLException {
		connection.close();
		System.out.println("[ACN - Database connection closed forcibly]");
	}

	public static Connection getConnection(String dbURL, String user, String password)
			throws SQLException, ClassNotFoundException {
		Class.forName("org.mariadb.jdbc.Driver");
		return DriverManager.getConnection(dbURL, user, password);
	}

	public void removeCoins(Player p, float amount) {
		Bukkit.getScheduler().runTaskAsynchronously(core, () ->{
				String UPDATE_QUERY = "UPDATE PlayerData SET Coins=? WHERE playerName=?";
				int coins = (int) (getCoins(p) - amount);
				try (PreparedStatement preparedStatement = getCurrentConnection().prepareStatement(UPDATE_QUERY)) {
					preparedStatement.setInt(1, coins);
					preparedStatement.setString(2, p.getUniqueId().toString());
					preparedStatement.executeUpdate();
				} catch (SQLException | ClassNotFoundException e) {
					e.printStackTrace();
				}
		});
	}

	public void removeCoins(UUID p, float amount) {
		Bukkit.getScheduler().runTaskAsynchronously(core, () ->{
				String UPDATE_QUERY = "UPDATE PlayerData SET Coins=? WHERE playerName=?";
				int coins = (int) (getCoins(p) - amount);
				try (PreparedStatement preparedStatement = getCurrentConnection().prepareStatement(UPDATE_QUERY)) {
					preparedStatement.setInt(1, coins);
					preparedStatement.setString(2, p.toString());
					preparedStatement.executeUpdate();
				} catch (SQLException | ClassNotFoundException e) {
					e.printStackTrace();
				}
		});
	}

	public void removeFromdata(UUID p) {
		try {
			connection = getCurrentConnection();
			Statement posted = connection.createStatement();
			posted.executeUpdate("DELETE FROM PlayerData WHERE playerName='" + p.toString() + "'");
			posted.close();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}

	public void addSkyWarsKills(Player p, float amount) {
		Bukkit.getScheduler().runTaskAsynchronously(core, () -> {
				String UPDATE_QUERY = "UPDATE PlayerData SET SWKills=? WHERE playerName=?";
				int kills = (int) (getSkyWarsKills(p) + amount);
				try (PreparedStatement preparedStatement = getCurrentConnection().prepareStatement(UPDATE_QUERY)) {
					preparedStatement.setInt(1, kills);
					preparedStatement.setString(2, p.getUniqueId().toString());
					preparedStatement.executeUpdate();
				} catch (SQLException | ClassNotFoundException e) {
					e.printStackTrace();
				}
		});
	}

	public void addLevel(Player p, float amount) {
		Bukkit.getScheduler().runTaskAsynchronously(core, () ->{
				String UPDATE_QUERY = "UPDATE PlayerData SET Level=? WHERE playerName=?";
				int level = (int) (getLevel(p) + amount);
				try (PreparedStatement preparedStatement = getCurrentConnection().prepareStatement(UPDATE_QUERY)) {
					preparedStatement.setInt(1, level);
					preparedStatement.setString(2, p.getUniqueId().toString());
					preparedStatement.executeUpdate();
				} catch (SQLException | ClassNotFoundException e) {
					e.printStackTrace();
				}
		});
	}

	public void addLevel(UUID p, float amount) {

		String UPDATE_QUERY = "UPDATE PlayerData SET Level=? WHERE playerName=?";
		int level = (int) (getLevel(p) + amount);
		try (PreparedStatement preparedStatement = getCurrentConnection().prepareStatement(UPDATE_QUERY)) {
			preparedStatement.setInt(1, level);
			preparedStatement.setString(2, p.toString());
			preparedStatement.executeUpdate();
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void addCoins(Player p, int amount) {
		Bukkit.getScheduler().runTaskAsynchronously(core, () ->{
				String UPDATE_QUERY = "UPDATE PlayerData SET Coins=? WHERE playerName=?";
				int coins = getCoins(p) + amount;
				try (PreparedStatement preparedStatement = getCurrentConnection().prepareStatement(UPDATE_QUERY)) {
					preparedStatement.setInt(1, coins);
					preparedStatement.setString(2, p.getUniqueId().toString());
					preparedStatement.executeUpdate();
				} catch (SQLException | ClassNotFoundException e) {
					e.printStackTrace();
				}
		});
	}

	public void addCoins(UUID p, int amount) {
		Bukkit.getScheduler().runTaskAsynchronously(core, () -> {
			String UPDATE_QUERY = "UPDATE PlayerData SET Coins=? WHERE playerName=?";
			int coins = getCoins(p) + amount;
			try (PreparedStatement preparedStatement = getCurrentConnection().prepareStatement(UPDATE_QUERY)) {
				preparedStatement.setInt(1, coins);
				preparedStatement.setString(2, p.toString());
				preparedStatement.executeUpdate();
			} catch (SQLException | ClassNotFoundException e) {
				e.printStackTrace();
			}
		});
	}

	public float getSkyWarsKills(Player p) {
		String query = "SELECT SWKills FROM PlayerData WHERE playerName=?";
		try (PreparedStatement posted = getCurrentConnection().prepareStatement(query)) {
			posted.setString(1, p.getUniqueId().toString());
			ResultSet resultSet = posted.executeQuery();
			while (resultSet.next()) {
				return resultSet.getFloat("SWKills");
			}
			return 0;
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public void addSkyWarsWins(Player p, float amount) {
		Bukkit.getScheduler().runTaskAsynchronously(core, () -> {
				String UPDATE_QUERY = "UPDATE PlayerData SET SWWins=? WHERE playerName=?";
				int wins = (int) (getSkyWarsWins(p) + amount);
				try (PreparedStatement preparedStatement = getCurrentConnection().prepareStatement(UPDATE_QUERY)) {
					preparedStatement.setInt(1, wins);
					preparedStatement.setString(2, p.getUniqueId().toString());
					preparedStatement.executeUpdate();
				} catch (SQLException | ClassNotFoundException e) {
					e.printStackTrace();
				}
		});
	}

	public float getSkyWarsWins(Player p) {
		String query = "SELECT SWWins FROM PlayerData WHERE playerName=?";
		try (PreparedStatement posted = getCurrentConnection().prepareStatement(query)) {
			posted.setString(1, p.getUniqueId().toString());
			ResultSet resultSet = posted.executeQuery();
			while (resultSet.next()) {
				return resultSet.getFloat("SWWins");
			}
			return 0;
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public float getLevel(Player p) {
		String query = "SELECT Level FROM PlayerData WHERE playerName=?";
		try (PreparedStatement posted = getCurrentConnection().prepareStatement(query)) {
			posted.setString(1, p.getUniqueId().toString());
			ResultSet resultSet = posted.executeQuery();
			while (resultSet.next()) {
				return resultSet.getFloat("Level");
			}
			return 0;
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public float getLevel(UUID p) {
		String query = "SELECT Level FROM PlayerData WHERE playerName=?";
		try (PreparedStatement posted = getCurrentConnection().prepareStatement(query)) {
			posted.setString(1, p.toString());
			ResultSet resultSet = posted.executeQuery();
			while (resultSet.next()) {
				return resultSet.getFloat("Level");
			}
			return 0;
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public void addSkyWarsDeaths(Player p, float amount) {
		Bukkit.getScheduler().runTaskAsynchronously(core, () -> {
				String UPDATE_QUERY = "UPDATE PlayerData SET SWDeaths=? WHERE playerName=?";
				int deaths = (int) (getSkyWarsDeaths(p) + amount);
				try (PreparedStatement preparedStatement = getCurrentConnection().prepareStatement(UPDATE_QUERY)) {
					preparedStatement.setInt(1, deaths);
					preparedStatement.setString(2, p.getUniqueId().toString());
					preparedStatement.executeUpdate();
				} catch (SQLException | ClassNotFoundException e) {
					e.printStackTrace();
				}
		});
	}

	public float getSkyWarsDeaths(Player p) {
		String query = "SELECT SWDeaths FROM PlayerData WHERE playerName=?";
		try (PreparedStatement posted = getCurrentConnection().prepareStatement(query)) {
			posted.setString(1, p.getUniqueId().toString());
			ResultSet resultSet = posted.executeQuery();
			while (resultSet.next()) {
				return resultSet.getFloat("SWDeaths");
			}
			return 0;
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public void addSkyWarsKit(Player p, String kit) {
		Bukkit.getScheduler().runTaskAsynchronously(core, () ->{
				String UPDATE_QUERY = "UPDATE PlayerData SET SWKits=? WHERE playerName=?";
				String kits = getSkyWarsKits(p) + kit + " ";
				try (PreparedStatement preparedStatement = getCurrentConnection().prepareStatement(UPDATE_QUERY)) {
					preparedStatement.setString(1, kits);
					preparedStatement.setString(2, p.getUniqueId().toString());
					preparedStatement.executeUpdate();
				} catch (SQLException | ClassNotFoundException e) {
					e.printStackTrace();
				}
		});
	}

	public void setName(Player p, int id) {

		String UPDATE_QUERY = "UPDATE PlayerData SET Name=? WHERE playerName=?";
		try (PreparedStatement preparedStatement = getCurrentConnection().prepareStatement(UPDATE_QUERY)) {
			preparedStatement.setString(1, p.getName());
			preparedStatement.setString(2, p.getUniqueId().toString());
			preparedStatement.executeUpdate();
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}

		try {
			connection = getCurrentConnection();
			Statement posted = connection.createStatement();
			posted.executeUpdate("UPDATE PlayerData SET Name='" + p.getName() + "' WHERE playerName='"
					+ p.getUniqueId().toString() + "'");
			System.out.println("[ACN - DATABASE " + p.getUniqueId().toString() + " Name information]");
			posted.close();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}

	public void setLanguage(Player p, int id) {
		Bukkit.getScheduler().runTaskAsynchronously(core, () ->{
				String UPDATE_QUERY = "UPDATE PlayerData SET Language=? WHERE playerName=?";
				try (PreparedStatement preparedStatement = getCurrentConnection().prepareStatement(UPDATE_QUERY)) {
					preparedStatement.setInt(1, id);
					preparedStatement.setString(2, p.getUniqueId().toString());
					preparedStatement.executeUpdate();
				} catch (SQLException | ClassNotFoundException e) {
					e.printStackTrace();
				}
		});
	}

	public void setBanned(Player p, boolean bool) {
		int banned = bool ? 1 : 0;
		Bukkit.getScheduler().runTaskAsynchronously(core, () ->{
			String UPDATE_QUERY = "UPDATE PlayerData SET ban=? WHERE playerName=?";
			try (PreparedStatement preparedStatement = getCurrentConnection().prepareStatement(UPDATE_QUERY)) {
				preparedStatement.setInt(1, banned);
				preparedStatement.setString(2, p.getUniqueId().toString());
				preparedStatement.executeUpdate();
			} catch (SQLException | ClassNotFoundException e) {
				e.printStackTrace();
			}
		});
	}

	public void setBanned(OfflinePlayer p, boolean bool) {
		int banned = bool ? 1 : 0;
		Bukkit.getScheduler().runTaskAsynchronously(core, () ->{
			String UPDATE_QUERY = "UPDATE PlayerData SET ban=? WHERE playerName=?";
			try (PreparedStatement preparedStatement = getCurrentConnection().prepareStatement(UPDATE_QUERY)) {
				preparedStatement.setInt(1, banned);
				preparedStatement.setString(2, p.getUniqueId().toString());
				preparedStatement.executeUpdate();
			} catch (SQLException | ClassNotFoundException e) {
				e.printStackTrace();
			}
		});
	}

	public void setbanTime(Player p, LocalDate time) {
		DateTimeFormatter formated = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		String newTime = time.format(formated);
		Bukkit.getScheduler().runTaskAsynchronously(core, () ->{
			String UPDATE_QUERY = "UPDATE PlayerData SET bantime=? WHERE playerName=?";
			try (PreparedStatement preparedStatement = getCurrentConnection().prepareStatement(UPDATE_QUERY)) {
				preparedStatement.setString(1, newTime);
				preparedStatement.setString(2, p.getUniqueId().toString());
				preparedStatement.executeUpdate();
			} catch (SQLException | ClassNotFoundException e) {
				e.printStackTrace();
			}
		});
	}

	public void setbanTime(OfflinePlayer p, LocalDate time) {
		DateTimeFormatter formated = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		String newTime = time.format(formated);
		Bukkit.getScheduler().runTaskAsynchronously(core, () ->{
			String UPDATE_QUERY = "UPDATE PlayerData SET bantime=? WHERE playerName=?";
			try (PreparedStatement preparedStatement = getCurrentConnection().prepareStatement(UPDATE_QUERY)) {
				preparedStatement.setString(1, newTime);
				preparedStatement.setString(2, p.getUniqueId().toString());
				preparedStatement.executeUpdate();
			} catch (SQLException | ClassNotFoundException e) {
				e.printStackTrace();
			}
		});
	}

	public void setCage(Player p, int id) {
		Bukkit.getScheduler().runTaskAsynchronously(core, () -> {
				String UPDATE_QUERY = "UPDATE PlayerData SET CAGE=? WHERE playerName=?";
				try (PreparedStatement preparedStatement = getCurrentConnection().prepareStatement(UPDATE_QUERY)) {
					preparedStatement.setInt(1, id);
					preparedStatement.setString(2, p.getUniqueId().toString());
					preparedStatement.executeUpdate();
				} catch (SQLException | ClassNotFoundException e) {
					e.printStackTrace();
				}
		});
	}

	public void setSelectedKit(Player p, String id) {
		Bukkit.getScheduler().runTaskAsynchronously(core, () ->{
				String UPDATE_QUERY = "UPDATE PlayerData SET SelectedKit=? WHERE playerName=?";
				try (PreparedStatement preparedStatement = getCurrentConnection().prepareStatement(UPDATE_QUERY)) {
					preparedStatement.setString(1, id);
					preparedStatement.setString(2, p.getUniqueId().toString());
					preparedStatement.executeUpdate();
				} catch (SQLException | ClassNotFoundException e) {
					e.printStackTrace();
				}
		});
	}

	public void setBanReason(Player p, String id) {
		Bukkit.getScheduler().runTaskAsynchronously(core, () ->{
			String UPDATE_QUERY = "UPDATE PlayerData SET reason=? WHERE playerName=?";
			try (PreparedStatement preparedStatement = getCurrentConnection().prepareStatement(UPDATE_QUERY)) {
				preparedStatement.setString(1, id);
				preparedStatement.setString(2, p.getUniqueId().toString());
				preparedStatement.executeUpdate();
			} catch (SQLException | ClassNotFoundException e) {
				e.printStackTrace();
			}
		});
	}

	public void setBanReason(OfflinePlayer p, String id) {
		Bukkit.getScheduler().runTaskAsynchronously(core, () ->{
			String UPDATE_QUERY = "UPDATE PlayerData SET reason=? WHERE playerName=?";
			try (PreparedStatement preparedStatement = getCurrentConnection().prepareStatement(UPDATE_QUERY)) {
				preparedStatement.setString(1, id);
				preparedStatement.setString(2, p.getUniqueId().toString());
				preparedStatement.executeUpdate();
			} catch (SQLException | ClassNotFoundException e) {
				e.printStackTrace();
			}
		});
	}

	public String getName(Player p) {
		String query = "SELECT Name FROM PlayerData WHERE playerName=?";
		try (PreparedStatement posted = getCurrentConnection().prepareStatement(query)) {
			posted.setString(1, p.getUniqueId().toString());
			ResultSet resultSet = posted.executeQuery();
			while (resultSet.next()) {
				return resultSet.getString("Name");
			}
			return null;
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	public String getSelectedKit(Player p) {
		String query = "SELECT SelectedKit FROM PlayerData WHERE playerName=?";
		try (PreparedStatement posted = getCurrentConnection().prepareStatement(query)) {
			posted.setString(1, p.getUniqueId().toString());
			ResultSet resultSet = posted.executeQuery();
			while (resultSet.next()) {
				return resultSet.getString("SelectedKit");
			}
			return null;
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	public String getBannedDate(Player p) {
		String query = "SELECT bantime FROM PlayerData WHERE playerName=?";
		try (PreparedStatement posted = getCurrentConnection().prepareStatement(query)) {
			posted.setString(1, p.getUniqueId().toString());
			ResultSet resultSet = posted.executeQuery();
			while (resultSet.next()) {
				return resultSet.getString("bantime");
			}
			return null;
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	public int getCage(Player p) {
		String query = "SELECT CAGE FROM PlayerData WHERE playerName=?";
		try (PreparedStatement posted = getCurrentConnection().prepareStatement(query)) {
			posted.setString(1, p.getUniqueId().toString());
			ResultSet resultSet = posted.executeQuery();
			while (resultSet.next()) {
				return resultSet.getInt("CAGE");
			}
			return 0;
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public int isBanned(Player p) {
		String query = "SELECT ban FROM PlayerData WHERE playerName=?";
		try (PreparedStatement posted = getCurrentConnection().prepareStatement(query)) {
			posted.setString(1, p.getUniqueId().toString());
			ResultSet resultSet = posted.executeQuery();
			while (resultSet.next()) {
				return resultSet.getInt("ban");
			}
			return 0;
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public void createStatements(Player p) {
		if (getName(p) != null) {
			core.getStatsCache().cacheUser(p);
		} else {
			String query = "INSERT INTO PlayerData (playerName, Name, Coins, SWKills, SWDeaths, Level, SWWins, SWKits, SelectedKit, CAGE, Language, ban, bantime, reason) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement st;

			try {
				connection = getCurrentConnection();
				st = connection.prepareStatement(query);
				st.setString(1, p.getUniqueId().toString());
				st.setString(2, p.getName());
				st.setInt(3, 250);
				st.setInt(4, 0);
				st.setInt(5, 0);
				st.setInt(6, 0);
				st.setInt(7, 0);
				st.setString(8, "");
				st.setString(9, "Noob");
				st.setInt(10, 1);
				st.setInt(11, 0);
				st.setInt(12, 0);
				st.setString(13, "NONE");
				st.setString(14, "NONE");
				st.executeUpdate();
				st.close();
			} catch (SQLException | ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
	}

	public int getCoins(Player p) {
		String query = "SELECT Coins FROM PlayerData WHERE playerName=?";
		try (PreparedStatement posted = getCurrentConnection().prepareStatement(query)) {
			posted.setString(1, p.getUniqueId().toString());
			ResultSet resultSet = posted.executeQuery();
			while (resultSet.next()) {
				return resultSet.getInt("Coins");
			}
			return 0;
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public int getLanguage(Player p) {
		String query = "SELECT Language FROM PlayerData WHERE playerName=?";
		try (PreparedStatement posted = getCurrentConnection().prepareStatement(query)) {
			posted.setString(1, p.getUniqueId().toString());
			ResultSet resultSet = posted.executeQuery();
			while (resultSet.next()) {
				return resultSet.getInt("Language");
			}
			return 0;
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public int getCoins(UUID p) {
		String query = "SELECT Coins FROM PlayerData WHERE playerName=?";
		try (PreparedStatement posted = getCurrentConnection().prepareStatement(query)) {
			posted.setString(1, p.toString());
			ResultSet resultSet = posted.executeQuery();
			while (resultSet.next()) {
				return resultSet.getInt("Coins");
			}
			return 0;
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public String getSkyWarsKits(Player p) {
		String query = "SELECT SWKits FROM PlayerData WHERE playerName=?";
		try (PreparedStatement posted = getCurrentConnection().prepareStatement(query)) {
			posted.setString(1, p.getUniqueId().toString());
			ResultSet resultSet = posted.executeQuery();
			while (resultSet.next()) {
				return resultSet.getString("SWKits");
			}
			return "";
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return "";
	}

	public String getBannedReason(Player p) {
		String query = "SELECT reason FROM PlayerData WHERE playerName=?";
		try (PreparedStatement posted = getCurrentConnection().prepareStatement(query)) {
			posted.setString(1, p.getUniqueId().toString());
			ResultSet resultSet = posted.executeQuery();
			while (resultSet.next()) {
				return resultSet.getString("reason");
			}
			return "";
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return "";
	}

	public ArrayList<String> getTopPlayer() throws ClassNotFoundException {
		int limit = 3;

		String name = null;

		ArrayList<String> names = new ArrayList<String>();
		try {
			connection = getCurrentConnection();
			PreparedStatement st = connection
					.prepareStatement("select * from PlayerData order by SWKills desc limit " + limit);
			ResultSet rs = st.executeQuery();
			if (rs.next()) {
				name = rs.getString("Name");
				names.add(name);
			} else {
				name = "No user found for first place";
			}
			if (rs.next()) {
				name = rs.getString("Name");
				names.add(name);
			} else {
				name = "No user found for second place";
			}
			if (rs.next()) {
				name = rs.getString("Name");
				names.add(name);
			} else {
				name = "No user found for third place";
			}
			if (names.size() == 1) {
				names.add("No user found.");
				names.add("No user found");
			} else if (names.size() == 2) {
				names.add("No user found.");
			} else if (names.size() == 3) {
				names.add("No user found.");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return names;
	}

	public ArrayList<Integer> getTopPlayerKills() throws ClassNotFoundException {
		int limit = 3;

		int name = 0;

		ArrayList<Integer> names = new ArrayList<Integer>();

		try {
			connection = getCurrentConnection();
			PreparedStatement st = connection
					.prepareStatement("select * from PlayerData order by SWKills desc limit " + limit);
			ResultSet rs = st.executeQuery();
			if (rs.next()) {
				name = rs.getInt("SWKills");
				names.add(Integer.valueOf(name));
			} else {
				name = 0;
			}
			if (rs.next()) {
				name = rs.getInt("SWKills");
				names.add(Integer.valueOf(name));
			} else {
				name = 0;
			}
			if (rs.next()) {
				name = rs.getInt("SWKills");
				names.add(Integer.valueOf(name));
			} else {
				name = 0;
			}
			if (names.size() == 1) {
				names.add(Integer.valueOf(2));
				names.add(Integer.valueOf(3));
			} else if (names.size() == 2) {
				names.add(Integer.valueOf(3));
			} else if (names.size() == 3) {
				names.add(Integer.valueOf(3));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return names;
	}

	public ArrayList<String> getTopDeathsPlayerName() throws ClassNotFoundException {
		int limit = 3;

		String name = null;

		ArrayList<String> names = new ArrayList<String>();
		try {
			connection = getCurrentConnection();
			PreparedStatement st = connection
					.prepareStatement("select * from PlayerData order by SWDeaths desc limit " + limit);
			ResultSet rs = st.executeQuery();
			if (rs.next()) {
				name = rs.getString("Name");
				names.add(name);
			} else {
				name = "No user found for first place";
			}
			if (rs.next()) {
				name = rs.getString("Name");
				names.add(name);
			} else {
				name = "No user found for second place";
			}
			if (rs.next()) {
				name = rs.getString("Name");
				names.add(name);
			} else {
				name = "No user found for third place";
			}
			if (names.size() == 1) {
				names.add("No user found.");
				names.add("No user found");
			} else if (names.size() == 2) {
				names.add("No user found.");
			} else if (names.size() == 3) {
				names.add("No user found.");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return names;
	}

	public ArrayList<Integer> getTopPlayerDeaths() throws ClassNotFoundException {
		int limit = 3;

		int name = 0;

		ArrayList<Integer> names = new ArrayList<Integer>();

		try {
			connection = getCurrentConnection();
			PreparedStatement st = connection
					.prepareStatement("select * from PlayerData order by SWDeaths desc limit " + limit);
			ResultSet rs = st.executeQuery();
			if (rs.next()) {
				name = rs.getInt("SWDeaths");
				names.add(Integer.valueOf(name));
			} else {
				name = 0;
			}
			if (rs.next()) {
				name = rs.getInt("SWDeaths");
				names.add(Integer.valueOf(name));
			} else {
				name = 0;
			}
			if (rs.next()) {
				name = rs.getInt("SWDeaths");
				names.add(Integer.valueOf(name));
			} else {
				name = 0;
			}
			if (names.size() == 1) {
				names.add(Integer.valueOf(2));
				names.add(Integer.valueOf(3));
			} else if (names.size() == 2) {
				names.add(Integer.valueOf(3));
			} else if (names.size() == 3) {
				names.add(Integer.valueOf(3));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return names;
	}

	public ArrayList<String> getTopWinsPlayerName() throws ClassNotFoundException {
		int limit = 3;

		String name = null;

		ArrayList<String> names = new ArrayList<String>();
		try {
			connection = getCurrentConnection();
			PreparedStatement st = connection
					.prepareStatement("select * from PlayerData order by SWWins desc limit " + limit);
			ResultSet rs = st.executeQuery();
			if (rs.next()) {
				name = rs.getString("Name");
				names.add(name);
			} else {
				name = "No user found for first place";
			}
			if (rs.next()) {
				name = rs.getString("Name");
				names.add(name);
			} else {
				name = "No user found for second place";
			}
			if (rs.next()) {
				name = rs.getString("Name");
				names.add(name);
			} else {
				name = "No user found for third place";
			}
			if (names.size() == 1) {
				names.add("No user found.");
				names.add("No user found");
			} else if (names.size() == 2) {
				names.add("No user found.");
			} else if (names.size() == 3) {
				names.add("No user found.");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return names;
	}

	public ArrayList<Integer> getTopPlayerWins() throws ClassNotFoundException {
		int limit = 3;

		int name = 0;

		ArrayList<Integer> names = new ArrayList<Integer>();

		try {
			connection = getCurrentConnection();
			PreparedStatement st = connection
					.prepareStatement("select * from PlayerData order by SWWins desc limit " + limit);
			ResultSet rs = st.executeQuery();
			if (rs.next()) {
				name = rs.getInt("SWWins");
				names.add(Integer.valueOf(name));
			} else {
				name = 0;
			}
			if (rs.next()) {
				name = rs.getInt("SWWins");
				names.add(Integer.valueOf(name));
			} else {
				name = 0;
			}
			if (rs.next()) {
				name = rs.getInt("SWWins");
				names.add(Integer.valueOf(name));
			} else {
				name = 0;
			}
			if (names.size() == 1) {
				names.add(Integer.valueOf(2));
				names.add(Integer.valueOf(3));
			} else if (names.size() == 2) {
				names.add(Integer.valueOf(3));
			} else if (names.size() == 3) {
				names.add(Integer.valueOf(3));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return names;
	}
}
