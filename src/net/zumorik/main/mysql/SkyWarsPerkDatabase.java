package net.zumorik.main.mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import net.zumorik.main.Core;

public class SkyWarsPerkDatabase {

	private PreparedStatement preparedStmt;
	private Core core;

	private String host;
	private String password;
	private String username;
	private String db;
	private Connection connection;

	public SkyWarsPerkDatabase(Core core) {
		this.core = core;
		host = core.getConfig().getString("host");
		password = core.getConfig().getString("swperks_password");
		username = core.getConfig().getString("swperks_user");
		db = core.getConfig().getString("swperks");

		try {
			connection = getConnection("jdbc:mariadb://" + host + "/" + db, username, password);
			this.preparedStmt = connection.prepareStatement(
					"CREATE TABLE IF NOT EXISTS PlayerData (playerName varchar(36) PRIMARY KEY NOT NULL,Name varchar(20), fire INT, ender INT, heal INT, speed INT, xp INT, armor INT, food INT, chests INT, cages varchar(2500), selectedCage varchar(30))");
			this.preparedStmt.executeUpdate();
			this.preparedStmt.close();
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public Connection getCurrentConnection() throws ClassNotFoundException, SQLException {
		if (connection != null) {
			if (connection.isValid(100)) {
				return connection;
			}else
			connection = getConnection("jdbc:mariadb://" + host + "/" + db, username, password);
			return connection;
		}else
		connection = getConnection("jdbc:mariadb://" + host + "/" + db, username, password);
		return connection;
	}

	public void close() throws SQLException {
		connection.close();
		System.out.println("[ACN - Database connection closed forcibly]");
	}

	public static Connection getConnection(String dbURL, String user, String password)
			throws SQLException, ClassNotFoundException {
		Class.forName("org.mariadb.jdbc.Driver");
		return DriverManager.getConnection(dbURL, user, password);
	}

	public void setPerk(Player p, String perk, int lvl) {
		Bukkit.getScheduler().runTaskAsynchronously(core, () ->{
				String UPDATE_QUERY = "UPDATE PlayerData SET " + perk + "=? WHERE playerName=?";
				try (PreparedStatement preparedStatement = getCurrentConnection().prepareStatement(UPDATE_QUERY)) {
					preparedStatement.setInt(1, lvl);
					preparedStatement.setString(2, p.getUniqueId().toString());
					preparedStatement.executeUpdate();
				} catch (SQLException | ClassNotFoundException e) {
					e.printStackTrace();
				}
		});
	}

	public void addCage(Player p, int cage) {
		Bukkit.getScheduler().runTaskAsynchronously(core, () -> {
				String cages = getCages(p) + cage + "|";
				String UPDATE_QUERY = "UPDATE PlayerData SET cages=? WHERE playerName=?";
				try (PreparedStatement preparedStatement = getCurrentConnection().prepareStatement(UPDATE_QUERY)) {
					preparedStatement.setString(1, cages);
					preparedStatement.setString(2, p.getUniqueId().toString());
					preparedStatement.executeUpdate();
				} catch (SQLException | ClassNotFoundException e) {
					e.printStackTrace();
				}
		});
	}

	public void setSelectedCage(Player p, int cage) {
		Bukkit.getScheduler().runTaskAsynchronously(core, () -> {
				String cages = "|" + cage + "|";
				String UPDATE_QUERY = "UPDATE PlayerData SET selectedCage=? WHERE playerName=?";
				try (PreparedStatement preparedStatement = getCurrentConnection().prepareStatement(UPDATE_QUERY)) {
					preparedStatement.setString(1, cages);
					preparedStatement.setString(2, p.getUniqueId().toString());
					preparedStatement.executeUpdate();
				} catch (SQLException | ClassNotFoundException e) {
					e.printStackTrace();
				}
		});
	}

	public int getPerk(Player p, String perk) {
		String query = "SELECT " + perk + " FROM PlayerData WHERE playerName=?";
		try (PreparedStatement posted = getCurrentConnection().prepareStatement(query)) {
			posted.setString(1, p.getUniqueId().toString());
			ResultSet resultSet = posted.executeQuery();
			while (resultSet.next()) {
				return resultSet.getInt(perk);
			}
			return 0;
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public String getCages(Player p) {
		String query = "SELECT cages FROM PlayerData WHERE playerName=?";
		try (PreparedStatement posted = getCurrentConnection().prepareStatement(query)) {
			posted.setString(1, p.getUniqueId().toString());
			ResultSet resultSet = posted.executeQuery();
			while (resultSet.next()) {
				return resultSet.getString("cages");
			}
			return "|1|";
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return "|1|";
	}

	public String getSelectedCage(Player p) {
		String query = "SELECT selectedCage FROM PlayerData WHERE playerName=?";
		try (PreparedStatement posted = getCurrentConnection().prepareStatement(query)) {
			posted.setString(1, p.getUniqueId().toString());
			ResultSet resultSet = posted.executeQuery();
			while (resultSet.next()) {
				return resultSet.getString("selectedCage");
			}
			return "|1|";
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return "|1|";
	}

	public int getChest(Player p, String perk) {
		String query = "SELECT " + perk + " FROM PlayerData WHERE playerName=?";
		try (PreparedStatement posted = getCurrentConnection().prepareStatement(query)) {
			posted.setString(1, p.getUniqueId().toString());
			ResultSet resultSet = posted.executeQuery();
			while (resultSet.next()) {
				return resultSet.getInt(perk);
			}
			return 0;
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return 0;
	}

	public void setName(Player p, int id) {
		String UPDATE_QUERY = "UPDATE PlayerData SET Name=? WHERE playerName=?";
		try (PreparedStatement preparedStatement = getCurrentConnection().prepareStatement(UPDATE_QUERY)) {
			preparedStatement.setString(1, p.getName());
			preparedStatement.setString(2, p.getUniqueId().toString());
			preparedStatement.executeUpdate();
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public String getName(Player p) {
		String query = "SELECT Name FROM PlayerData WHERE playerName=?";
		try (PreparedStatement posted = getCurrentConnection().prepareStatement(query)) {
			posted.setString(1, p.getUniqueId().toString());
			ResultSet resultSet = posted.executeQuery();
			while (resultSet.next()) {
				return resultSet.getString("Name");
			}
			return null;
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	public void createStatements(Player p) {
		if (getName(p) != null) {
			core.getSkywarsPerkCache().cacheUser(p);
		} else {
			try {
				connection = getCurrentConnection();
				String query = "INSERT INTO PlayerData (playerName, Name, fire, ender, heal, speed, xp, armor, food, chests, cages, selectedCage) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
				PreparedStatement st = connection.prepareStatement(query);
				st.setString(1, p.getUniqueId().toString());
				st.setString(2, p.getName());
				st.setInt(3, 0);
				st.setInt(4, 0);
				st.setInt(5, 0);
				st.setInt(6, 0);
				st.setInt(7, 0);
				st.setInt(8, 0);
				st.setInt(9, 0);
				st.setInt(10, 0);
				st.setString(11, "|1|");
				st.setString(12, "|1|");
				st.executeUpdate();
				st.close();
			} catch (SQLException | ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
	}
}
