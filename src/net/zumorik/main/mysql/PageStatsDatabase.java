package net.zumorik.main.mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.lang.RandomStringUtils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import net.zumorik.main.Core;

public class PageStatsDatabase {

	private Core core;
	private PreparedStatement preparedStmt;
	private String host;
	private String password;
	private String username;
	private String db;
	private Connection connection;

	public String base = "NULL";

	public PageStatsDatabase(Core core) {
		this.core = core;
		host = core.getConfig().getString("pageStats_host");
		password = core.getConfig().getString("pageStats_password");
		username = core.getConfig().getString("pageStats_user");
		db = core.getConfig().getString("pageStats");
		if (base.equalsIgnoreCase("NULL")) {
			try {
				connection = getConnection("jdbc:mariadb://" + host + "/" + db, username, password);
				base = randomString();
				this.preparedStmt = connection.prepareStatement("CREATE TABLE IF NOT EXISTS " + base
						+ " (playerName varchar(36) PRIMARY KEY NOT NULL ,name varchar(20),GAMEID varchar(50), KILLS INT, KIT varchar(500), WON INT)");
				this.preparedStmt.executeUpdate();
				this.preparedStmt.close();
			} catch (ClassNotFoundException | SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public String randomString() {
		return RandomStringUtils.randomAlphabetic(10);
	}

	public Connection getCurrentConnection() throws ClassNotFoundException, SQLException {
		if (connection != null) {
			if (connection.isValid(100)) {
				return connection;
			}else
			connection = getConnection("jdbc:mariadb://" + host + "/" + db, username, password);
			return connection;
		}else
		connection = getConnection("jdbc:mariadb://" + host + "/" + db, username, password);
		return connection;
	}

	public void close() throws SQLException {
		connection.close();
		System.out.println("[ACN - Database connection closed forcibly]");
	}

	public static Connection getConnection(String dbURL, String user, String password)
			throws SQLException, ClassNotFoundException {
		Class.forName("org.mariadb.jdbc.Driver");
		return DriverManager.getConnection(dbURL, user, password);
	}

	// int kills = getInformation(p,"KILLS");
	// String kills = getInformation(p,"KILLS");
	// example setInformation(p, "KILLS", getInformation(p,"KILLS") + 1);
	public void setInformation(String p, String info, int data) {
		Bukkit.getScheduler().runTaskAsynchronously(core, () -> {
				String UPDATE_QUERY = "UPDATE " + base + " SET " + info + "=? WHERE playerName=?";
				try (PreparedStatement preparedStatement = getCurrentConnection().prepareStatement(UPDATE_QUERY)) {
					preparedStatement.setInt(1, data);
					preparedStatement.setString(2, p);
					preparedStatement.executeUpdate();
				} catch (SQLException | ClassNotFoundException e) {
					e.printStackTrace();
				}
		});
	}

	public void setInformation(String p, String info, String data) {
		Bukkit.getScheduler().runTaskAsynchronously(core, () ->{
				String UPDATE_QUERY = "UPDATE " + base + " SET " + info + "=? WHERE playerName=?";
				try (PreparedStatement preparedStatement = getCurrentConnection().prepareStatement(UPDATE_QUERY)) {
					preparedStatement.setString(1, data);
					preparedStatement.setString(2, p);
					preparedStatement.executeUpdate();
				} catch (SQLException | ClassNotFoundException e) {
					e.printStackTrace();
				}
		});
	}

	public void setName(Player p, int id) {
		String UPDATE_QUERY = "UPDATE " + base + " SET name=? WHERE playerName=?";
		try (PreparedStatement preparedStatement = getCurrentConnection().prepareStatement(UPDATE_QUERY)) {
			preparedStatement.setString(1, p.getName());
			preparedStatement.setString(2, p.getUniqueId().toString());
			preparedStatement.executeUpdate();
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public String getName(Player p) {
		String query = "SELECT name FROM " + base + " WHERE playerName=?";
		try (PreparedStatement posted = getCurrentConnection().prepareStatement(query)) {
			posted.setString(1, p.getUniqueId().toString());
			ResultSet resultSet = posted.executeQuery();
			while (resultSet.next()) {
				return resultSet.getString("name");
			}
			return null;
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	public void createStatements(Player p, String gameID, String kitName) {
		if (getName(p) == null) {

			try {
				connection = getCurrentConnection();
				String query = "INSERT INTO " + base + " (playerName,name,GAMEID,KILLS,KIT,WON) VALUES (?,?,?,?,?,?)";
				PreparedStatement st;
				st = connection.prepareStatement(query);
				st.setString(1, p.getName());
				st.setString(2, p.getName());
				st.setString(3, gameID);
				st.setInt(4, 0);
				st.setString(5, kitName);
				st.setInt(6, 0);
				st.executeUpdate();
				st.close();
			} catch (SQLException | ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
	}
}
