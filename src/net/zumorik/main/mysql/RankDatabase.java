package net.zumorik.main.mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import net.zumorik.main.Core;
import net.zumorik.main.rank.Rank;

public class RankDatabase {

	private PreparedStatement preparedStmt;
	private Core core;
	private String host;
	private String password;
	private String username;
	private String db;
	private static final String SEPARATOR = ",";
	private Connection connection;

	public RankDatabase(Core core) {
		this.core = core;
		host = core.getConfig().getString("host");
		password = core.getConfig().getString("rank_password");
		username = core.getConfig().getString("rank_user");
		db = core.getConfig().getString("rank");
		try {
			connection = getConnection("jdbc:mariadb://" + host + "/" + db, username, password);
			this.preparedStmt = connection.prepareStatement(
					"CREATE TABLE IF NOT EXISTS ranks (uuid varchar(36) PRIMARY KEY NOT NULL,rankid varchar(20), name varchar(30))");
			this.preparedStmt.executeUpdate();
			this.preparedStmt.close();
			this.preparedStmt = connection.prepareStatement(
					"CREATE TABLE IF NOT EXISTS globalperms (ranks varchar(36) PRIMARY KEY NOT NULL, rankperms varchar(1500))");
			this.preparedStmt.executeUpdate();
			this.preparedStmt.close();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}

	public Connection getCurrentConnection() throws ClassNotFoundException, SQLException {
		if (connection != null) {
			if (connection.isValid(100)) {
				return connection;
			}else
			connection = getConnection("jdbc:mariadb://" + host + "/" + db, username, password);
			return connection;
		}else
		connection = getConnection("jdbc:mariadb://" + host + "/" + db, username, password);
		return connection;
	}

	public void close() throws SQLException {
		connection.close();
		System.out.println("[ACN - Database connection closed forcibly]");
	}

	public static Connection getConnection(String dbURL, String user, String password)
			throws SQLException, ClassNotFoundException {
		Class.forName("org.mariadb.jdbc.Driver");
		return DriverManager.getConnection(dbURL, user, password);
	}

	public void setRank(Player p, String name) {
		String UPDATE_QUERY = "UPDATE ranks SET rankid=? WHERE uuid=?";
		try (PreparedStatement preparedStatement = getCurrentConnection().prepareStatement(UPDATE_QUERY)) {
			preparedStatement.setString(1, name);
			preparedStatement.setString(2, p.getUniqueId().toString());
			preparedStatement.executeUpdate();
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void setRank(OfflinePlayer p, String name) {

		String UPDATE_QUERY = "UPDATE ranks SET rankid=? WHERE uuid=?";
		try (PreparedStatement preparedStatement = getCurrentConnection().prepareStatement(UPDATE_QUERY)) {
			preparedStatement.setString(1, name);
			preparedStatement.setString(2, p.getUniqueId().toString());
			preparedStatement.executeUpdate();
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public String getRank(OfflinePlayer p) {
		String query = "SELECT rankid FROM ranks WHERE uuid=?";
		try (PreparedStatement posted = getCurrentConnection().prepareStatement(query)) {
			posted.setString(1, p.getUniqueId().toString());
			ResultSet resultSet = posted.executeQuery();
			while (resultSet.next()) {
				return resultSet.getString("rankid");
			}
			return "USER";
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return "USER";
	}

	public String getRank(Player p) {
		String query = "SELECT rankid FROM ranks WHERE uuid=?";
		try (PreparedStatement posted = getCurrentConnection().prepareStatement(query)) {
			posted.setString(1, p.getUniqueId().toString());
			ResultSet resultSet = posted.executeQuery();
			while (resultSet.next()) {
				return resultSet.getString("rankid");
			}
			return null;
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	public String getName(Player p) {
		String query = "SELECT name FROM ranks WHERE uuid=?";
		try (PreparedStatement posted = getCurrentConnection().prepareStatement(query)) {
			posted.setString(1, p.getUniqueId().toString());
			ResultSet resultSet = posted.executeQuery();
			while (resultSet.next()) {
				return resultSet.getString("name");
			}
			return null;
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	public void createRank(String rank) {
		try {
			connection = getCurrentConnection();
			String query = "INSERT INTO globalperms (ranks, rankperms) VALUES (?,?)";
			PreparedStatement st = connection.prepareStatement(query);
			st.setString(1, rank);
			st.setString(2, "");
			st.executeUpdate();
			st.close();
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public String getGlobalPerms(Rank rank) {
		String query = "SELECT rankperms FROM globalperms WHERE ranks=?";
		try (PreparedStatement posted = getCurrentConnection().prepareStatement(query)) {
			posted.setString(1, rank.toString().toUpperCase());
			ResultSet resultSet = posted.executeQuery();
			while (resultSet.next()) {
				return resultSet.getString("rankperms");
			}
			return null;
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	public void addGlobalPerm(String rank, String perm) {
		String UPDATE_QUERY = "UPDATE globalperms SET rankperms=? WHERE ranks=?";
		try (PreparedStatement preparedStatement = getCurrentConnection().prepareStatement(UPDATE_QUERY)) {
			String perms = getGlobalPerms(Rank.valueOf(rank));
			String perms1 = perms + "," + perm;
			preparedStatement.setString(1, perms1);
			preparedStatement.setString(2, rank);
			preparedStatement.executeUpdate();
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void removeGlobalPerm(String rank, String perm) {
		String UPDATE_QUERY = "UPDATE globalperms SET rankperms=? WHERE ranks=?";
		try (PreparedStatement preparedStatement = getCurrentConnection().prepareStatement(UPDATE_QUERY)) {
			String perms = getGlobalPerms(Rank.valueOf(rank));
			String[] p = perms.split(",");
			List<String> permz = new ArrayList<String>(Arrays.asList(p));
			if (permz.contains(perm)) {
				permz.remove(perm);
				StringBuilder csvBuilder = new StringBuilder();
				for (String z : permz) {
					csvBuilder.append(z);
					csvBuilder.append(SEPARATOR);
				}
				String csv = csvBuilder.toString();
				csv = csv.substring(0, csv.length() - SEPARATOR.length());
				preparedStatement.setString(1, csv);
				preparedStatement.setString(2, rank);
				preparedStatement.executeUpdate();
			}
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void createStatements(Player p) {
		if (getRank(p) != null && getName(p) != null) {
			core.getRankCache().translateRank(p);
		} else {
			try {
				connection = getCurrentConnection();
				String query = "INSERT INTO ranks (uuid, rankid, name) VALUES (?,?,?)";
				PreparedStatement st = connection.prepareStatement(query);
				st.setString(1, p.getUniqueId().toString());
				st.setString(2, "USER");
				st.setString(3, p.getName());
				st.executeUpdate();
				st.close();
				core.getRankCache().translateRank(p);
			} catch (SQLException | ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
	}
}
