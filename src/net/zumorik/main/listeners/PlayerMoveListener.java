package net.zumorik.main.listeners;

import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import net.zumorik.main.Core;
import net.zumorik.main.handlers.FreezeHandler;

public class PlayerMoveListener implements Listener {

	@SuppressWarnings("unused")
	private Core core;
	private FreezeHandler handler;

	public PlayerMoveListener(Core core) {
		this.core = core;
		handler = new FreezeHandler(core);
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onMove(PlayerMoveEvent event) {
		Player p = event.getPlayer();
		if (handler.getFroozenPlayers().contains(p.getName())) {
			Location from = event.getFrom();
			double xfrom = event.getFrom().getX();
			double yfrom = event.getFrom().getY();
			double zfrom = event.getFrom().getZ();
			double xto = event.getTo().getX();
			double yto = event.getTo().getY();
			double zto = event.getTo().getZ();
			if (!(xfrom == xto && yfrom == yto && zfrom == zto)) {
				p.teleport(from);
				p.playEffect(p.getLocation(), Effect.STEP_SOUND, 79);
			}
		}
	}

}
