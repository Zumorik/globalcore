package net.zumorik.main.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import net.zumorik.main.Core;
import net.zumorik.main.handlers.VanishHandler;

public class PlayerLeaveListeners implements Listener {

	private Core core;

	public PlayerLeaveListeners(Core core) {
		this.core = core;
	}

	@EventHandler
	public void onLeave(PlayerQuitEvent e) {
		Player p = e.getPlayer();
		core.getStatsCache().removeCacheUser(p);
		core.getMysteryCache().uncacheUser(p);
		core.getRankCache().uncacheUser(p);

		if (core.getConfig().getBoolean("sw")) {
			core.getSkywarsPerkCache().uncacheUser(p);
		}
		if (core.getConfig().getBoolean("swlobby")) {
			core.getSkywarsPerkCache().uncacheUser(p);
		}
		VanishHandler.players.remove(p.getName());
	}

	@EventHandler
	public void onKick(PlayerKickEvent e) {
		Player p = e.getPlayer();
		try {
			core.getStatsCache().removeCacheUser(p);
			core.getMysteryCache().uncacheUser(p);
			core.getSkywarsPerkCache().uncacheUser(p);
		} catch (NullPointerException es) {

		}
	}

}
