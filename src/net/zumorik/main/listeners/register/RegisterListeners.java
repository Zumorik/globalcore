package net.zumorik.main.listeners.register;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginManager;

import net.zumorik.main.Core;
import net.zumorik.main.listeners.PlayerChatListener;
import net.zumorik.main.listeners.PlayerJoinListener;
import net.zumorik.main.listeners.PlayerLeaveListeners;
import net.zumorik.main.listeners.PlayerMoveListener;

public class RegisterListeners {

	private Core core;
	private PluginManager pl;

	private int totalListeners;
	private int currentListener;

	public RegisterListeners(Core core) {
		this.core = core;
		totalListeners = 4;
		currentListener = 1;
		pl = Bukkit.getPluginManager();
	}

	public void registerListeners() {
		registerListener(new PlayerJoinListener(core));
		registerListener(new PlayerLeaveListeners(core));
		registerListener(new PlayerChatListener(core));
		registerListener(new PlayerMoveListener(core));
	}

	private void registerListener(Listener listener){
		Bukkit.getConsoleSender().sendMessage(colo("&aGlobalCore &8- &eRegistering Listener: &b(" + currentListener + "/" + totalListeners + ")"));
		pl.registerEvents(listener, core);
		currentListener++;
	}

	private String colo(String s){
		return ChatColor.translateAlternateColorCodes('&', s);
	}

}
