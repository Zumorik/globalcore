package net.zumorik.main.listeners;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import net.md_5.bungee.api.ChatColor;
import net.zumorik.main.Core;

public class PlayerChatListener implements Listener {

	private Core core;

	private Set<Player> antispam;
	private HashMap<Player, String> flood;

	public PlayerChatListener(Core core) {
		this.core = core;
		this.antispam = new HashSet<Player>();
		this.flood = new HashMap<Player, String>();
	}

	@EventHandler
	public void onChat(AsyncPlayerChatEvent e) {
		final Player p = e.getPlayer();
		if (this.core.getConfig().getBoolean("enabledChat")) {
			String rank;
			try {
				rank = this.core.getRankCache().getRank(p).toString();
			} catch (NullPointerException es) {
				rank = "USER";
			}
			if (this.core.getConfig().contains(rank + ".format")) {
				String format = this.core.getConfig().getString(rank + ".format");
				if ((!rank.contains("USER")) || (!rank.equalsIgnoreCase("USER"))) {
					e.setMessage(colo(e.getMessage()));
				}
				if (this.antispam.contains(p)) {
					e.setCancelled(true);
					p.sendMessage(
							colo("&a&lChat &8| &cYou have to wait &e3 &cseconds to chat."));
					return;
				}
				e.setFormat(colo(format));
				if ((core.getRankCache().hasBasicSystemPermissions(p))) {
					return;
				}
				this.antispam.add(p);
				int time = 3;
				Bukkit.getScheduler().scheduleSyncDelayedTask(this.core, () -> {
						PlayerChatListener.this.antispam.remove(p);
				}, time * 20);
			} else {
				if (this.antispam.contains(p)) {
					e.setCancelled(true);
					p.sendMessage(
							colo("&a&lChat &8| &cYou have to wait &e3 &cseconds to chat."));
					return;
				}
				e.setFormat(colo("&e%s &f%s"));
				if ((!core.getRankCache().hasBasicSystemPermissions(p))) {
					this.antispam.add(p);
					int time = 3;
					Bukkit.getScheduler().scheduleSyncDelayedTask(this.core, () ->{
							PlayerChatListener.this.antispam.remove(p);

					}, time * 20);
				}
			}
		}
	}

	private String colo(String s) {
		return ChatColor.translateAlternateColorCodes('&', s);
	}

	public Set<Player> getAntispam() {
		return antispam;
	}

	public void setAntispam(Set<Player> antispam) {
		this.antispam = antispam;
	}

	public HashMap<Player, String> getFlood() {
		return flood;
	}

	public void setFlood(HashMap<Player, String> flood) {
		this.flood = flood;
	}

}
