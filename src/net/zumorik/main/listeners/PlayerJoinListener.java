package net.zumorik.main.listeners;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import com.google.common.collect.Iterables;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import net.zumorik.bungeecord.punish.BanReasons;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import net.zumorik.main.Core;

public class PlayerJoinListener implements Listener {

	private Core core;

	public PlayerJoinListener(Core core) {
		this.core = core;
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent e) throws ClassNotFoundException, SQLException {
		Player p = e.getPlayer();
		Bukkit.getScheduler().runTaskAsynchronously(core, () -> {
			core.getStatsDatabase().createStatements(p);
			if(core.getStatsDatabase().isBanned(p) == 1){

				String banTimeString = core.getStatsDatabase().getBannedDate(p);

				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
				LocalDate date = LocalDate.parse(banTimeString,formatter);

				LocalDate now = LocalDate.now();
				if(date.getYear() == 1000){
					BanReasons reason = BanReasons.valueOf(core.getStatsDatabase().getBannedReason(p));
					sendMessage("ban:" + p.getName() + ":" + reason.toString() + ":" + banTimeString);
					System.out.println(p.getName() + "tried to join but they are permanently banned.");
					return;
				}else if(now.isAfter(date) || date.isEqual(now)){
					core.getStatsDatabase().setBanned(p, false);
					core.getStatsDatabase().setbanTime(p, LocalDate.now());
					core.getStatsDatabase().setBanReason(p, " ");
					System.out.println(p.getName() + " ban has expired and will be permitted to join.");

					Bukkit.getScheduler().scheduleSyncDelayedTask(core, () -> {

						if(core.getStatsDatabase().getLanguage(p) == 1){
							for(int i = 0; i < 50; i++){
								p.sendMessage(" ");
							}
							p.sendMessage(colo("          &b&lHiso&c&lPunishment          "));
							p.sendMessage(" ");
							p.sendMessage(" ");
							p.sendMessage(colo("&eYour ban has expired and has been placed in your &brecord&e."));
							p.sendMessage(colo("&eIf you wish not to get banned again please follow."));
							p.sendMessage(colo("&cALL &eNetwork rules."));
							p.sendMessage(" ");
							p.sendMessage(colo("&eBan date&7: &b" + date.toString()));
							p.sendMessage(" ");
							p.sendMessage(colo("&ahttp://hisocraft.com/rules"));
							p.sendMessage(" ");
							p.sendMessage(" ");
						}else{
							for(int i = 0; i < 50; i++){
								p.sendMessage(" ");
							}
							p.sendMessage(colo("          &b&lHiso&c&lPunishment          "));
							p.sendMessage(" ");
							p.sendMessage(" ");
							p.sendMessage(colo("&esu ban ha expirado y se ha incluido en su &bregistro."));
							p.sendMessage(colo("&eSi deseas no ser baneado, siga."));
							p.sendMessage(colo("&cTodas &elas reglas de la red."));
							p.sendMessage(" ");
							p.sendMessage(colo("&eFetcha de ban&7: &b" + date.toString()));
							p.sendMessage(" ");
							p.sendMessage(colo("&ahttp://hisocraft.com/rules"));
							p.sendMessage(" ");
							p.sendMessage(" ");
						}

					},60);


				}else{
					BanReasons reason = BanReasons.valueOf(core.getStatsDatabase().getBannedReason(p));
					sendMessage("ban:" + p.getName() + ":" + reason.toString() + ":" + banTimeString);
					System.out.println("ban:" + p.getName() + ":" + reason.toString() + ":" + date.toString());
					System.out.println(p.getName() + " attempted to join but they are banned until " + banTimeString);
				}

				return;
			}

			core.getRankDatabase().createStatements(p);

			if (core.getConfig().getBoolean("sw")) {
				core.getSkywarsPerkDatabase().createStatements(p);
			}
			if (core.getConfig().getBoolean("lobby")) {
				core.getMiscDatabase().createStatements(p);
				core.getDailyDatabase().createStatements(p);
			}
			if (core.getConfig().getBoolean("sglobby")) {
				core.getMiscDatabase().createStatements(p);
				core.getDailyDatabase().createStatements(p);
			}
			if (core.getConfig().getBoolean("swlobby")) {
				core.getMiscDatabase().createStatements(p);
				core.getSkywarsPerkDatabase().createStatements(p);
				core.getDailyDatabase().createStatements(p);
			}
		});
	}

	public void sendMessage(String code){
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		out.writeUTF(code);
		Player player = Iterables.getFirst(Bukkit.getOnlinePlayers(), null);
		player.sendPluginMessage(core, "Punish", out.toByteArray());
	}

	private String colo(String s){
		return ChatColor.translateAlternateColorCodes('&',s);
	}
}
