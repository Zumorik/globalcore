package net.zumorik.main.cache;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import net.zumorik.main.Core;
import net.zumorik.main.rank.Rank;

public class RankCache {

	private Core core;
	private HashMap<String, Rank> ranks;

	public RankCache(Core core) {
		this.core = core;
		this.ranks = new HashMap<String, Rank>();
	}

	public void translateRank(Player p) {
		String rank = "USER";
		String name = p.getName();
		rank = core.getRankDatabase().getRank(p);

		switch (rank) {

		case "USER":
			ranks.put(name, Rank.USER);
			break;
		case "VIP":
			ranks.put(name, Rank.VIP);
			break;
		case "ELITE":
			ranks.put(name, Rank.ELITE);
			break;
		case "ULTRA":
			ranks.put(name, Rank.ULTRA);
			break;
		case "LEGEND":
			ranks.put(name, Rank.LEGEND);
			break;
		case "EPIC":
			ranks.put(name, Rank.EPIC);
			break;
		case "ADMIN":
			ranks.put(name, Rank.ADMIN);
			break;
		case "MOD":
			ranks.put(name, Rank.MOD);
			break;
		case "OWNER":
			ranks.put(name, Rank.OWNER);
			break;
		case "SRMOD":
			ranks.put(name, Rank.SRMOD);
			break;
		case "SUBMOD":
			ranks.put(name, Rank.SUBMOD);
			break;
		case "SRHELPER":
			ranks.put(name, Rank.SRHELPER);
		case "HELPER":
			ranks.put(name, Rank.HELPER);
			break;
		case "YOUTUBER":
			ranks.put(name, Rank.YOUTUBER);
			break;
		case "DEV":
			ranks.put(name, Rank.DEV);
			break;
		case "SRBUILDER":
			ranks.put(name, Rank.SRBUILDER);
			break;
		case "BUILDER":
			ranks.put(name, Rank.BUILDER);
			break;
			case "STREAMER":
				ranks.put(name, Rank.STREAMER);
				break;
		default:
			ranks.put(name, Rank.USER);
			break;
		}
	}

	@SuppressWarnings("unlikely-arg-type")
	public Rank getRank(Player p) {
		if (ranks.containsKey(p.getName())) {
			return ranks.get(p.getName());
		} else {
			translateRank(p);
			return ranks.get(p);
		}
	}

	public boolean hasSuperiorSystemPermissions(Player p){
		boolean t = false;
		if(hasAdminPermissions(p))
			t = true;
		else if (hasModeratorPermissions(p))
			t = true;
		return t;
	}

	public boolean hasBasicSystemPermissions(Player p){
		boolean t = false;
		if(hasAdminPermissions(p))
			t = true;
		else if (hasModeratorPermissions(p))
			t = true;
		else if (hasHelperPermissions(p))
			t = true;
		return t;
	}

	public boolean isMedia(Player p){
		boolean t = false;
		if(getRank(p).equals(Rank.STREAMER) || getRank(p).equals(Rank.YOUTUBER))
			return true;
		else
			return false;
	}

	public boolean hasFlyPermissions(Player p){
		boolean t = false;
		if(hasAdminPermissions(p))
			t = true;
		else if (hasModeratorPermissions(p))
			t = true;
		else if (hasHelperPermissions(p))
			t = true;
		return t;
	}

	public boolean hasAdminPermissions(Player p){
		if(getRank(p).equals(Rank.ADMIN) || getRank(p).equals(Rank.OWNER))
			return true;
		else
			return false;
	}

	public boolean hasModeratorPermissions(Player p){
		if(getRank(p).equals(Rank.ADMIN) || getRank(p).equals(Rank.MOD) || getRank(p).equals(Rank.OWNER) || getRank(p).equals(Rank.SRMOD) || getRank(p).equals(Rank.SUBMOD))
			return true;
		else
			return false;
	}

	public boolean hasHelperPermissions(Player p){
		if(getRank(p).equals(Rank.BUILDER) || getRank(p).equals(Rank.MOD) || getRank(p).equals(Rank.HELPER) || getRank(p).equals(Rank.SRHELPER) ||
				getRank(p).equals(Rank.SRMOD) || getRank(p).equals(Rank.SUBMOD) || getRank(p).equals(Rank.YOUTUBER)|| getRank(p).equals(Rank.STREAMER) || getRank(p).equals(Rank.OWNER))
			return true;
		else
			return false;
	}

	public void setRank(final Player p, final Rank rank) {
		Bukkit.getScheduler().runTaskAsynchronously(core, () ->{
				ranks.put(p.getName(), rank);
				core.getRankDatabase().setRank(p, rank.toString());
		});
	}

	public void uncacheUser(Player p) {
		ranks.remove(p.getName());

	}
}
