package net.zumorik.main.cache;

import java.sql.SQLException;
import java.util.HashMap;

import org.bukkit.entity.Player;

import net.zumorik.main.Core;

public class MiscDataCache {

	private HashMap<String, Integer> normal, rare, legendary, speed, player, flight;

	public Core core;

	public MiscDataCache(Core core) {
		this.core = core;
		normal = new HashMap<String, Integer>();
		rare = new HashMap<String, Integer>();
		legendary = new HashMap<String, Integer>();
		speed = new HashMap<String, Integer>();
		player = new HashMap<String, Integer>();
		flight = new HashMap<String, Integer>();
	}

	public void addNormal(Player p, int amount)  {
		if (!core.getMysteryCache().getNormal().containsKey(p.getName())) {
			core.getMysteryCache().getNormal().put(p.getName(),
					(core.getMiscDatabase().getNormalMysteryBox(p) + amount));
			core.getMysteryCache().addNormal(p, amount);
		} else {
			core.getMysteryCache().getNormal().put(p.getName(),
					((int) core.getMiscDatabase().getNormalMysteryBox(p) + amount));
			core.getMiscDatabase().addNormalMysteryBox(p, amount);
		}

	}

	public int getSpeed(Player p)  {
		if (core.getMysteryCache().getSpeed().containsKey(p.getName())) {
			return core.getMysteryCache().getSpeed().get(p.getName());
		} else
			core.getMysteryCache().getSpeed().put(p.getName(), core.getMiscDatabase().getSpeed(p));
		return core.getMysteryCache().getSpeed().get(p.getName());
	}

	public int getFlight(Player p)  {
		if (core.getMysteryCache().getFlight().containsKey(p.getName())) {
			return core.getMysteryCache().getFlight().get(p.getName());
		} else
			core.getMysteryCache().getFlight().put(p.getName(), core.getMiscDatabase().getFlight(p));
		return core.getMysteryCache().getFlight().get(p.getName());
	}

	public int getPlayer(Player p)  {
		if (core.getMysteryCache().getPlayer().containsKey(p.getName())) {
			return core.getMysteryCache().getPlayer().get(p.getName());
		} else
			core.getMysteryCache().getPlayer().put(p.getName(), core.getMiscDatabase().getPlayer(p));
		return core.getMysteryCache().getPlayer().get(p.getName());
	}

	public void setSpeed(Player p, int setting) {
		core.getMysteryCache().getSpeed().put(p.getName(), setting);
		core.getMiscDatabase().setSpeed(p, setting);
	}

	public void setFlight(Player p, int setting) {
		core.getMysteryCache().getFlight().put(p.getName(), setting);
		core.getMiscDatabase().setFlight(p, setting);
	}

	public void setPlayer(Player p, int setting) {
		core.getMysteryCache().getPlayer().put(p.getName(), setting);
		core.getMiscDatabase().setPlayer(p, setting);
	}

	public void addRare(Player p, int amount)  {
		if (!core.getMysteryCache().getRare().containsKey(p.getName())) {
			core.getMysteryCache().getRare().put(p.getName(),
					((int) core.getMiscDatabase().getRareMysteryBox(p) + amount));
			core.getMysteryCache().addRare(p, amount);
		} else {
			core.getMysteryCache().getRare().put(p.getName(),
					((int) core.getMiscDatabase().getRareMysteryBox(p) + amount));
			core.getMiscDatabase().addRareMysteryBox(p, amount);
		}

	}

	public void addLegendary(Player p, int amount)  {
		if (!core.getMysteryCache().getLegendary().containsKey(p.getName())) {
			core.getMysteryCache().getLegendary().put(p.getName(),
					((int) core.getMiscDatabase().getLegMysteryBox(p) + amount));
			core.getMysteryCache().addLegendary(p, amount);
		} else {
			core.getMysteryCache().getLegendary().put(p.getName(),
					((int) core.getMiscDatabase().getLegMysteryBox(p) + amount));
			core.getMiscDatabase().addLegMysteryBox(p, amount);
		}

	}

	public void removeNormal(Player p, int amount)  {

		if (!core.getMysteryCache().getNormal().containsKey(p.getName())) {
			core.getMysteryCache().getNormal().put(p.getName(), (getNormal(p) - amount));
			core.getMiscDatabase().removeNormalMysteryBox(p, amount);
		} else {
			core.getMysteryCache().getNormal().put(p.getName(),
					((int) core.getMiscDatabase().getNormalMysteryBox(p) - amount));
			core.getMiscDatabase().removeNormalMysteryBox(p, amount);
		}

	}

	public void removeRare(Player p, int amount) {

		if (!core.getMysteryCache().getRare().containsKey(p.getName())) {
			core.getMysteryCache().getRare().put(p.getName(), (getRare(p) - amount));
			core.getMiscDatabase().removeRareMysteryBox(p, amount);
		} else {
			core.getMysteryCache().getRare().put(p.getName(),
					((int) core.getMiscDatabase().getRareMysteryBox(p) - amount));
			core.getMiscDatabase().removeRareMysteryBox(p, amount);
		}

	}

	public void removeLegendary(Player p, int amount)  {

		if (!core.getMysteryCache().getLegendary().containsKey(p.getName())) {
			core.getMysteryCache().getLegendary().put(p.getName(), (getLegendary(p) - amount));
			core.getMiscDatabase().removeLegMysteryBox(p, amount);
		} else {
			core.getMysteryCache().getLegendary().put(p.getName(),
					((int) core.getMiscDatabase().getLegMysteryBox(p) - amount));
			core.getMiscDatabase().removeLegMysteryBox(p, amount);
		}

	}

	public int getNormal(Player p)  {
		if (core.getMysteryCache().getNormal().containsKey(p.getName())) {
			return core.getMysteryCache().getNormal().get(p.getName());
		} else
			core.getMysteryCache().getNormal().put(p.getName(),
					((int) core.getMiscDatabase().getNormalMysteryBox(p)));
		return core.getMysteryCache().getNormal().get(p.getName());
	}

	public int getRare(Player p)  {
		if (core.getMysteryCache().getRare().containsKey(p.getName())) {
			return core.getMysteryCache().getRare().get(p.getName());
		} else
			core.getMysteryCache().getRare().put(p.getName(),
					((int) core.getMiscDatabase().getRareMysteryBox(p)));
		return core.getMysteryCache().getRare().get(p.getName());
	}

	public int getLegendary(Player p) {
		if (core.getMysteryCache().getLegendary().containsKey(p.getName())) {
			return core.getMysteryCache().getLegendary().get(p.getName());
		} else
			core.getMysteryCache().getLegendary().put(p.getName(),
					((int) core.getMiscDatabase().getLegMysteryBox(p)));
		return core.getMysteryCache().getLegendary().get(p.getName());
	}

	public void cacheUser(Player p) {
			core.getMysteryCache().getNormal().put(p.getName(), getNormal(p));
			core.getMysteryCache().getRare().put(p.getName(), getRare(p));
			core.getMysteryCache().getLegendary().put(p.getName(), getLegendary(p));
			core.getMysteryCache().getSpeed().put(p.getName(), core.getMiscDatabase().getSpeed(p));
			core.getMysteryCache().getFlight().put(p.getName(), core.getMiscDatabase().getFlight(p));
			core.getMysteryCache().getPlayer().put(p.getName(), core.getMiscDatabase().getPlayer(p));
	}

	public void uncacheUser(Player p) {
		core.getMysteryCache().getNormal().remove(p.getName());
		core.getMysteryCache().getRare().remove(p.getName());
		core.getMysteryCache().getLegendary().remove(p.getName());
		core.getMysteryCache().getSpeed().remove(p.getName());
		core.getMysteryCache().getFlight().remove(p.getName());
		core.getMysteryCache().getPlayer().remove(p.getName());
	}

	public HashMap<String, Integer> getPlayer() {
		return player;
	}

	public void setPlayer(HashMap<String, Integer> player) {
		this.player = player;
	}

	public HashMap<String, Integer> getFlight() {
		return flight;
	}

	public void setFlight(HashMap<String, Integer> flight) {
		this.flight = flight;
	}

	public HashMap<String, Integer> getSpeed() {
		return speed;
	}

	public void setSpeed(HashMap<String, Integer> speed) {
		this.speed = speed;
	}

	public HashMap<String, Integer> getNormal() {
		return normal;
	}

	public HashMap<String, Integer> getLegendary() {
		return legendary;
	}

	public HashMap<String, Integer> getRare() {
		return rare;
	}

}
