package net.zumorik.main.cache;

import java.sql.SQLException;
import java.util.HashMap;

import org.bukkit.entity.Player;

import net.zumorik.main.Core;

public class SkyWarsPerkCache {

	private Core core;

	private HashMap<String, Integer> firecache, endercache, healcache, speedcache, xpcache, armorcache, chests, foodcache;
	private HashMap<String, String> cages, selectedCage;

	public SkyWarsPerkCache(Core core) {
		this.core = core;
		firecache = new HashMap<String, Integer>();
		endercache = new HashMap<String, Integer>();
		healcache = new HashMap<String, Integer>();
		speedcache = new HashMap<String, Integer>();
		xpcache = new HashMap<String, Integer>();
		armorcache = new HashMap<String, Integer>();
		foodcache = new HashMap<String, Integer>();
		chests = new HashMap<String, Integer>();
		setCages(new HashMap<String, String>());
		setSelectedCage(new HashMap<String, String>());
	}

	public HashMap<String, Integer> getFirecache() {
		return firecache;
	}

	public void setFirecache(HashMap<String, Integer> firecache) {
		this.firecache = firecache;
	}

	public HashMap<String, Integer> getEndercache() {
		return endercache;
	}

	public void setEndercache(HashMap<String, Integer> endercache) {
		this.endercache = endercache;
	}

	public HashMap<String, Integer> getHealcache() {
		return healcache;
	}

	public void setHealcache(HashMap<String, Integer> healcache) {
		this.healcache = healcache;
	}

	public HashMap<String, Integer> getSpeedcache() {
		return speedcache;
	}

	public void setSpeedcache(HashMap<String, Integer> speedcache) {
		this.speedcache = speedcache;
	}

	public HashMap<String, Integer> getXpcache() {
		return xpcache;
	}

	public void setXpcache(HashMap<String, Integer> xpcache) {
		this.xpcache = xpcache;
	}

	public HashMap<String, Integer> getArmorcache() {
		return armorcache;
	}

	public void setArmorcache(HashMap<String, Integer> armorcache) {
		this.armorcache = armorcache;
	}

	public HashMap<String, Integer> getFoodcache() {
		return foodcache;
	}

	public void setFoodcache(HashMap<String, Integer> foodcache) {
		this.foodcache = foodcache;
	}

	public int getCachedChests(Player p) throws ClassNotFoundException, SQLException {
		if (core.getSkywarsPerkCache().getChests().containsKey(p.getName())) {
			return core.getSkywarsPerkCache().getChests().get(p.getName());
		} else
			core.getSkywarsPerkCache().getChests().put(p.getName(), core.getSkywarsPerkDatabase().getPerk(p, "chests"));
		return core.getSkywarsPerkCache().getChests().get(p.getName());
	}

	public String getCachedCages(Player p) throws ClassNotFoundException, SQLException {
		if (core.getSkywarsPerkCache().getCages().containsKey(p.getName())) {
			if (core.getSkywarsPerkCache().getCages().get(p.getName()) == null) {
				core.getSkywarsPerkCache().addCage(p, 1);
				return "|1|";
			} else
				return core.getSkywarsPerkCache().getCages().get(p.getName());
		} else
			core.getSkywarsPerkCache().getCages().put(p.getName(), core.getSkywarsPerkDatabase().getCages(p));
		if (core.getSkywarsPerkCache().getCages().get(p.getName()) == null) {
			core.getSkywarsPerkCache().addCage(p, 1);
			return "|1|";
		} else
			return core.getSkywarsPerkCache().getCages().get(p.getName());
	}

	public String getCachedSelectedCage(Player p) throws ClassNotFoundException, SQLException {
		if (core.getSkywarsPerkCache().getSelectedCage().containsKey(p.getName())) {
			if (core.getSkywarsPerkCache().getSelectedCage().get(p.getName()) == null) {
				return "|1|";
			} else
				return core.getSkywarsPerkCache().getSelectedCage().get(p.getName());
		} else
			core.getSkywarsPerkCache().getSelectedCage().put(p.getName(),
					core.getSkywarsPerkDatabase().getSelectedCage(p));
		if (core.getSkywarsPerkCache().getSelectedCage().get(p.getName()) == null) {
			return "|1|";
		} else
			return core.getSkywarsPerkCache().getSelectedCage().get(p.getName());
	}

	public void addChests(Player p, int i) throws ClassNotFoundException, SQLException {
		if (core.getSkywarsPerkCache().getChests().containsKey(p.getName())) {
			core.getSkywarsPerkCache().getChests().put(p.getName(),
					core.getSkywarsPerkDatabase().getPerk(p, "chests") + 1);
			core.getSkywarsPerkDatabase().setPerk(p, "chests", getCachedChests(p));
		} else {
			core.getSkywarsPerkCache().getChests().put(p.getName(),
					core.getSkywarsPerkDatabase().getPerk(p, "chests") + 1);
			core.getSkywarsPerkDatabase().setPerk(p, "chests", getCachedChests(p));
		}
	}

	public void addCage(Player p, int i) throws ClassNotFoundException, SQLException {
		if (core.getSkywarsPerkCache().getCages().containsKey(p.getName())) {
			core.getSkywarsPerkCache().getCages().put(p.getName(), core.getSkywarsPerkDatabase().getCages(p) + i + "|");
			core.getSkywarsPerkDatabase().addCage(p, i);
		} else {
			core.getSkywarsPerkCache().getCages().put(p.getName(),
					core.getSkywarsPerkCache().getCages().get(p.getName()) + i + "|");
			core.getSkywarsPerkDatabase().addCage(p, i);
		}
	}

	public void setSelectedCage(Player p, int i) throws ClassNotFoundException, SQLException {
		if (core.getSkywarsPerkCache().getSelectedCage().containsKey(p.getName())) {
			core.getSkywarsPerkCache().getSelectedCage().put(p.getName(), "|" + i + "|");
			core.getSkywarsPerkDatabase().setSelectedCage(p, i);
		} else {
			core.getSkywarsPerkCache().getSelectedCage().put(p.getName(), "|" + i + "|");
			core.getSkywarsPerkDatabase().setSelectedCage(p, i);
		}
	}

	public void removeChests(Player p, int i) throws ClassNotFoundException, SQLException {
		if (core.getSkywarsPerkCache().getChests().containsKey(p.getName())) {
			core.getSkywarsPerkCache().getChests().put(p.getName(),
					core.getSkywarsPerkDatabase().getPerk(p, "chests") - 1);
			core.getSkywarsPerkDatabase().setPerk(p, "chests", getCachedChests(p));
		} else {
			core.getSkywarsPerkCache().getChests().put(p.getName(),
					core.getSkywarsPerkDatabase().getPerk(p, "chests") - 1);
			core.getSkywarsPerkDatabase().setPerk(p, "chests", getCachedChests(p));
		}
	}

	public int getFireLvl(Player p) throws ClassNotFoundException, SQLException {
		if (core.getSkywarsPerkCache().getFirecache().containsKey(p.getName())) {
			return core.getSkywarsPerkCache().getFirecache().get(p.getName());
		} else
			core.getSkywarsPerkCache().getFirecache().put(p.getName(),
					core.getSkywarsPerkDatabase().getPerk(p, "fire"));
		return core.getSkywarsPerkCache().getFirecache().get(p.getName());
	}

	public void addFireLvl(Player p, int i) throws ClassNotFoundException, SQLException {
		if (core.getSkywarsPerkCache().getFirecache().containsKey(p.getName())) {
			core.getSkywarsPerkCache().getFirecache().put(p.getName(),
					core.getSkywarsPerkDatabase().getPerk(p, "fire") + 1);
			core.getSkywarsPerkDatabase().setPerk(p, "fire", getFireLvl(p));
		} else {
			core.getSkywarsPerkCache().getFirecache().put(p.getName(),
					core.getSkywarsPerkDatabase().getPerk(p, "fire") + 1);
			core.getSkywarsPerkDatabase().setPerk(p, "fire", getFireLvl(p));
		}
	}

	public int getEnderLvl(Player p) throws ClassNotFoundException, SQLException {
		if (core.getSkywarsPerkCache().getEndercache().containsKey(p.getName())) {
			System.out.println("ID 1");
			return core.getSkywarsPerkCache().getEndercache().get(p.getName());
		} else
			core.getSkywarsPerkCache().getEndercache().put(p.getName(),
					core.getSkywarsPerkDatabase().getPerk(p, "ender"));
		System.out.println("ID 2");
		return core.getSkywarsPerkCache().getEndercache().get(p.getName());
	}

	public void addEnderLvl(Player p, int i) throws ClassNotFoundException, SQLException {
		if (core.getSkywarsPerkCache().getEndercache().containsKey(p.getName())) {
			core.getSkywarsPerkCache().getEndercache().put(p.getName(),
					core.getSkywarsPerkDatabase().getPerk(p, "ender") + 1);
			core.getSkywarsPerkDatabase().setPerk(p, "ender", getEnderLvl(p));
		} else {
			core.getSkywarsPerkCache().getEndercache().put(p.getName(),
					core.getSkywarsPerkDatabase().getPerk(p, "ender") + 1);
			core.getSkywarsPerkDatabase().setPerk(p, "ender", getEnderLvl(p));
		}
	}

	public int getHealLvl(Player p) throws ClassNotFoundException, SQLException {
		if (core.getSkywarsPerkCache().getHealcache().containsKey(p.getName())) {
			return core.getSkywarsPerkCache().getHealcache().get(p.getName());
		} else
			core.getSkywarsPerkCache().getHealcache().put(p.getName(),
					core.getSkywarsPerkDatabase().getPerk(p, "heal"));
		return core.getSkywarsPerkCache().getHealcache().get(p.getName());
	}

	public void addHealLvl(Player p, int i) throws ClassNotFoundException, SQLException {
		if (core.getSkywarsPerkCache().getHealcache().containsKey(p.getName())) {
			core.getSkywarsPerkCache().getHealcache().put(p.getName(),
					core.getSkywarsPerkDatabase().getPerk(p, "heal") + 1);
			core.getSkywarsPerkDatabase().setPerk(p, "heal", getHealLvl(p));
		} else {
			core.getSkywarsPerkCache().getHealcache().put(p.getName(),
					core.getSkywarsPerkDatabase().getPerk(p, "heal") + 1);
			core.getSkywarsPerkDatabase().setPerk(p, "heal", getHealLvl(p));
		}
	}

	public int getSpeedLvl(Player p) throws ClassNotFoundException, SQLException {
		if (core.getSkywarsPerkCache().getSpeedcache().containsKey(p.getName())) {
			return core.getSkywarsPerkCache().getSpeedcache().get(p.getName());
		} else
			core.getSkywarsPerkCache().getSpeedcache().put(p.getName(),
					core.getSkywarsPerkDatabase().getPerk(p, "speed"));
		return core.getSkywarsPerkCache().getSpeedcache().get(p.getName());
	}

	public void addSpeedLvl(Player p, int i) throws ClassNotFoundException, SQLException {
		if (core.getSkywarsPerkCache().getSpeedcache().containsKey(p.getName())) {
			core.getSkywarsPerkCache().getSpeedcache().put(p.getName(),
					core.getSkywarsPerkDatabase().getPerk(p, "speed") + 1);
			core.getSkywarsPerkDatabase().setPerk(p, "speed", getSpeedLvl(p));
		} else {
			core.getSkywarsPerkCache().getSpeedcache().put(p.getName(),
					core.getSkywarsPerkDatabase().getPerk(p, "speed") + 1);
			core.getSkywarsPerkDatabase().setPerk(p, "speed", getSpeedLvl(p));
		}
	}

	public int getXpLvl(Player p) throws ClassNotFoundException, SQLException {
		if (core.getSkywarsPerkCache().getXpcache().containsKey(p.getName())) {
			return core.getSkywarsPerkCache().getXpcache().get(p.getName());
		} else
			core.getSkywarsPerkCache().getXpcache().put(p.getName(), core.getSkywarsPerkDatabase().getPerk(p, "xp"));
		return core.getSkywarsPerkCache().getXpcache().get(p.getName());
	}

	public void addXpLvl(Player p, int i) throws ClassNotFoundException, SQLException {
		if (core.getSkywarsPerkCache().getXpcache().containsKey(p.getName())) {
			core.getSkywarsPerkCache().getXpcache().put(p.getName(),
					core.getSkywarsPerkDatabase().getPerk(p, "xp") + 1);
			core.getSkywarsPerkDatabase().setPerk(p, "xp", getXpLvl(p));
		} else {
			core.getSkywarsPerkCache().getXpcache().put(p.getName(),
					core.getSkywarsPerkDatabase().getPerk(p, "armor") + 1);
			core.getSkywarsPerkDatabase().setPerk(p, "xp", getXpLvl(p));
		}
	}

	public int getArmorLvl(Player p) throws ClassNotFoundException, SQLException {
		if (core.getSkywarsPerkCache().getArmorcache().containsKey(p.getName())) {
			return core.getSkywarsPerkCache().getArmorcache().get(p.getName());
		} else
			core.getSkywarsPerkCache().getArmorcache().put(p.getName(),
					core.getSkywarsPerkDatabase().getPerk(p, "armor"));
		return core.getSkywarsPerkCache().getArmorcache().get(p.getName());
	}

	public void addArmorLvl(Player p, int i) throws ClassNotFoundException, SQLException {
		if (core.getSkywarsPerkCache().getArmorcache().containsKey(p.getName())) {
			core.getSkywarsPerkCache().getArmorcache().put(p.getName(),
					core.getSkywarsPerkDatabase().getPerk(p, "armor") + 1);
			core.getSkywarsPerkDatabase().setPerk(p, "armor", getArmorLvl(p));
		} else {
			core.getSkywarsPerkCache().getArmorcache().put(p.getName(),
					core.getSkywarsPerkDatabase().getPerk(p, "armor") + 1);
			core.getSkywarsPerkDatabase().setPerk(p, "armor", getArmorLvl(p));
		}
	}

	public int getFoodLvl(Player p) throws ClassNotFoundException, SQLException {
		if (core.getSkywarsPerkCache().getFoodcache().containsKey(p.getName())) {
			return core.getSkywarsPerkCache().getFoodcache().get(p.getName());
		} else
			core.getSkywarsPerkCache().getFoodcache().put(p.getName(),
					core.getSkywarsPerkDatabase().getPerk(p, "food"));
		return core.getSkywarsPerkCache().getFoodcache().get(p.getName());
	}

	public void addFoodLvl(Player p, int i) throws ClassNotFoundException, SQLException {
		if (core.getSkywarsPerkCache().getFoodcache().containsKey(p.getName())) {
			core.getSkywarsPerkCache().getFoodcache().put(p.getName(),
					core.getSkywarsPerkDatabase().getPerk(p, "food") + 1);
			core.getSkywarsPerkDatabase().setPerk(p, "food", getFoodLvl(p));
		} else {
			core.getSkywarsPerkCache().getFoodcache().put(p.getName(),
					core.getSkywarsPerkDatabase().getPerk(p, "food") + 1);
			core.getSkywarsPerkDatabase().setPerk(p, "food", getFoodLvl(p));
		}
	}

	public void cacheUser(Player p) {
		core.getSkywarsPerkCache().getFirecache().put(p.getName(), core.getSkywarsPerkDatabase().getPerk(p, "fire"));
		core.getSkywarsPerkCache().getEndercache().put(p.getName(), core.getSkywarsPerkDatabase().getPerk(p, "ender"));
		core.getSkywarsPerkCache().getHealcache().put(p.getName(), core.getSkywarsPerkDatabase().getPerk(p, "heal"));
		core.getSkywarsPerkCache().getSpeedcache().put(p.getName(), core.getSkywarsPerkDatabase().getPerk(p, "speed"));
		core.getSkywarsPerkCache().getXpcache().put(p.getName(), core.getSkywarsPerkDatabase().getPerk(p, "xp"));
		core.getSkywarsPerkCache().getArmorcache().put(p.getName(), core.getSkywarsPerkDatabase().getPerk(p, "armor"));
		core.getSkywarsPerkCache().getFoodcache().put(p.getName(), core.getSkywarsPerkDatabase().getPerk(p, "food"));
		core.getSkywarsPerkCache().getChests().put(p.getName(), core.getSkywarsPerkDatabase().getPerk(p, "chests"));
		core.getSkywarsPerkCache().getCages().put(p.getName(), core.getSkywarsPerkDatabase().getCages(p));
		core.getSkywarsPerkCache().getSelectedCage().put(p.getName(), core.getSkywarsPerkDatabase().getSelectedCage(p));
	}

	public void uncacheUser(Player p) {
		core.getSkywarsPerkCache().getFirecache().remove(p.getName());
		core.getSkywarsPerkCache().getEndercache().remove(p.getName());
		core.getSkywarsPerkCache().getHealcache().remove(p.getName());
		core.getSkywarsPerkCache().getSpeedcache().remove(p.getName());
		core.getSkywarsPerkCache().getXpcache().remove(p.getName());
		core.getSkywarsPerkCache().getArmorcache().remove(p.getName());
		core.getSkywarsPerkCache().getFoodcache().remove(p.getName());
		core.getSkywarsPerkCache().getChests().remove(p.getName());
		core.getSkywarsPerkCache().getCages().remove(p.getName());
		core.getSkywarsPerkCache().getSelectedCage().remove(p.getName());
	}

	public HashMap<String, Integer> getChests() {
		return chests;
	}

	public void setChests(HashMap<String, Integer> chests) {
		this.chests = chests;
	}

	public HashMap<String, String> getCages() {
		return cages;
	}

	public void setCages(HashMap<String, String> cages) {
		this.cages = cages;
	}

	public HashMap<String, String> getSelectedCage() {
		return selectedCage;
	}

	public void setSelectedCage(HashMap<String, String> selectedCage) {
		this.selectedCage = selectedCage;
	}
}
