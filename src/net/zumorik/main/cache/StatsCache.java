package net.zumorik.main.cache;

import java.sql.SQLException;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import net.zumorik.main.Core;

public class StatsCache {

	private final Core core;

	private final HashMap<String, Integer> coins;
	private final HashMap<String, Integer> language;
	private final HashMap<String, Integer> skywarsKills;
	private final HashMap<String, Integer> skywarsDeaths;
	private final HashMap<String, Integer> skywarsCage;
	private final HashMap<String, Integer> skywarsWins;
	private HashMap<String, String> skywarsSelectedKit;
	private final HashMap<String, String> swKits;
	private final HashMap<String, Float> level;

	public StatsCache(Core core) {
		this.core = core;
		coins = new HashMap<>();
		language = new HashMap<>();
		skywarsKills = new HashMap<>();
		skywarsDeaths = new HashMap<>();
		skywarsSelectedKit = new HashMap<>();
		skywarsCage = new HashMap<>();
		skywarsWins = new HashMap<>();
		level = new HashMap<>();
		swKits = new HashMap<>();
	}

	public void cacheUser(Player p) {
		core.getStatsCache().getCachedCoins().put(p.getName(), (int) core.getStatsDatabase().getCoins(p));
		core.getStatsCache().getCachedSkyWarsKills().put(p.getName(), (int) core.getStatsDatabase().getSkyWarsKills(p));
		core.getStatsCache().getCachedSkyWarsDeaths().put(p.getName(),
				(int) core.getStatsDatabase().getSkyWarsDeaths(p));
		core.getStatsCache().getCachedLevel().put(p.getName(), core.getStatsDatabase().getLevel(p));
		core.getStatsCache().getCachedSkyWarsWins().put(p.getName(), (int) core.getStatsDatabase().getSkyWarsWins(p));
		core.getStatsCache().getCachedSkyWarsKits().put(p.getName(), core.getStatsDatabase().getSkyWarsKits(p));
		core.getStatsCache().getCachedSkyWarsKits().put(p.getName(), core.getStatsDatabase().getSkyWarsKits(p));
		core.getStatsCache().getSkywarsSelectedKit().put(p.getName(), core.getStatsDatabase().getSelectedKit(p));
		core.getStatsCache().getCachedLanguage().put(p.getName(), core.getStatsDatabase().getLanguage(p));
	}

	public void addCoins(Player p, int amount)  {
		int coins = core.getStatsDatabase().getCoins(p);
		int newAmount = coins + amount;
		core.getStatsCache().getCachedCoins().put(p.getName(), newAmount);
		Bukkit.getScheduler().runTaskAsynchronously(core, () -> core.getStatsDatabase().addCoins(p, amount));
	}

	public void addSkyWarsWins(Player p, int amount) {
		core.getStatsCache().getCachedSkyWarsWins().put(p.getName(),
				((int) core.getStatsDatabase().getSkyWarsWins(p) + amount));
		Bukkit.getScheduler().runTaskAsynchronously(core, () ->{
		core.getStatsDatabase().addSkyWarsWins(p, amount);});
	}

	public void removeCoins(Player p, int amount) {
		core.getStatsCache().getCachedCoins().put(p.getName(), ((int) core.getStatsDatabase().getCoins(p) - amount));
		Bukkit.getScheduler().runTaskAsynchronously(core, () -> core.getStatsDatabase().removeCoins(p, amount));
	}

	public void setLangauge(Player p, int id) {
		core.getStatsCache().getCachedLanguage().put(p.getName(), id);
		Bukkit.getScheduler().runTaskAsynchronously(core, () -> core.getStatsDatabase().setLanguage(p, id));
	}

	public void setSelectedKit(Player p, String amount) {
		core.getStatsCache().getSkywarsSelectedKit().put(p.getName(), amount);
		Bukkit.getScheduler().runTaskAsynchronously(core, () ->core.getStatsDatabase().setSelectedKit(p, amount));
	}

	public int getCoins(Player p) {
		if (core.getStatsCache().getCachedCoins().containsKey(p.getName())) {
			return core.getStatsCache().getCachedCoins().get(p.getName());
		} else
			core.getStatsCache().getCachedCoins().put(p.getName(), core.getStatsDatabase().getCoins(p));
		return core.getStatsCache().getCachedCoins().get(p.getName());
	}

	public int getLanguage(Player p) {
		if (core.getStatsCache().getCachedLanguage().containsKey(p.getName())) {
			return core.getStatsCache().getCachedLanguage().get(p.getName());
		} else
			core.getStatsCache().getCachedLanguage().put(p.getName(), core.getStatsDatabase().getLanguage(p));
		return core.getStatsCache().getCachedLanguage().get(p.getName());
	}

	public int getSkyWarsWins(Player p) {
		if (core.getStatsCache().getCachedSkyWarsWins().containsKey(p.getName())) {
			return core.getStatsCache().getCachedSkyWarsWins().get(p.getName());
		} else
			core.getStatsCache().getCachedSkyWarsWins().put(p.getName(),
					((int) core.getStatsDatabase().getSkyWarsWins(p)));
		return core.getStatsCache().getCachedSkyWarsWins().get(p.getName());
	}

	public String getSkyWarsSelectedKit(Player p) {
		if (core.getStatsCache().getSkywarsSelectedKit().containsKey(p.getName())) {
			return core.getStatsCache().getSkywarsSelectedKit().get(p.getName());
		} else
			core.getStatsCache().getSkywarsSelectedKit().put(p.getName(), (core.getStatsDatabase().getSelectedKit(p)));
		return core.getStatsCache().getSkywarsSelectedKit().get(p.getName());
	}

	public int getSkyWarsKills(Player p) {
		if (core.getStatsCache().getCachedSkyWarsKills().containsKey(p.getName())) {
			return core.getStatsCache().getCachedSkyWarsKills().get(p.getName());
		} else
			core.getStatsCache().getCachedSkyWarsKills().put(p.getName(),
					((int) core.getStatsDatabase().getSkyWarsKills(p)));
		return core.getStatsCache().getCachedSkyWarsKills().get(p.getName());
	}

	public void addSkyWarsKills(Player p, int amount) {
		core.getStatsCache().getCachedSkyWarsKills().put(p.getName(),
				((int) core.getStatsDatabase().getSkyWarsKills(p) + amount));
		Bukkit.getScheduler().runTaskAsynchronously(core, () -> core.getStatsDatabase().addSkyWarsKills(p, amount));
	}

	public int getSWCage(Player p) {
		if (core.getStatsCache().getCachedSWCage().containsKey(p.getName())) {
			return core.getStatsCache().getCachedSWCage().get(p.getName());
		} else
			core.getStatsCache().getCachedSWCage().put(p.getName(), ((int) core.getStatsDatabase().getCage(p)));
		return core.getStatsCache().getCachedSWCage().get(p.getName());
	}

	public void setSWCage(Player p, int id) {
		core.getStatsCache().getCachedSWCage().put(p.getName(), ((int) core.getStatsDatabase().getCage(p)));
		Bukkit.getScheduler().runTaskAsynchronously(core, () -> core.getStatsDatabase().setCage(p, id));
	}

	public int getSkyWarsDeaths(Player p)  {
		if (core.getStatsCache().getCachedSkyWarsDeaths().containsKey(p.getName())) {
			return core.getStatsCache().getCachedSkyWarsDeaths().get(p.getName());
		} else
			core.getStatsCache().getCachedSkyWarsDeaths().put(p.getName(),
					((int) core.getStatsDatabase().getSkyWarsDeaths(p)));
		return core.getStatsCache().getCachedSkyWarsDeaths().get(p.getName());
	}

	public void addSkyWarsDeaths(Player p, int amount)  {
		core.getStatsCache().getCachedSkyWarsDeaths().put(p.getName(),
				((int) core.getStatsDatabase().getSkyWarsDeaths(p) + amount));
		Bukkit.getScheduler().runTaskAsynchronously(core, () -> core.getStatsDatabase().addSkyWarsDeaths(p, amount));
	}

	public void removeCacheUser(Player p) {

		core.getStatsCache().getCachedCoins().remove(p.getName());
		core.getStatsCache().getCachedSkyWarsDeaths().remove(p.getName());
		core.getStatsCache().getCachedSkyWarsKills().remove(p.getName());
		core.getStatsCache().getCachedSkyWarsKits().remove(p.getName());

	}

	public int getLevel(Player p)  {

		if (core.getStatsCache().getCachedLevel().containsKey(p.getName())) {
			return ((int) core.getStatsDatabase().getLevel(p) / 1000);
		} else
			core.getStatsCache().getCachedLevel().put(p.getName(),
					core.getStatsDatabase().getLevel(p));
		return ((int) core.getStatsDatabase().getLevel(p) / 1000);
	}

	public void addXP(Player p, float amount) {
		core.getStatsCache().getCachedLevel().put(p.getName(), (core.getStatsDatabase().getLevel(p) + amount));
		Bukkit.getScheduler().runTaskAsynchronously(core, () -> core.getStatsDatabase().addLevel(p, amount));
	}

	public void addSkyWarsKits(Player p, String kit) {
		core.getStatsCache().getCachedSkyWarsKits().put(p.getName(),
				(core.getStatsDatabase().getSkyWarsKits(p) + kit + " "));
		Bukkit.getScheduler().runTaskAsynchronously(core, () ->
		core.getStatsDatabase().addSkyWarsKit(p, kit));
	}

	public String getSkyWarsKit(Player p) {
		if (core.getStatsCache().getCachedSkyWarsKits().containsKey(p.getName())) {
			return core.getStatsCache().getCachedSkyWarsKits().get(p.getName());
		} else
			core.getStatsCache().getCachedSkyWarsKits().put(p.getName(), (core.getStatsDatabase().getSkyWarsKits(p)));
		return core.getStatsCache().getCachedSkyWarsKits().get(p.getName());
	}

	public HashMap<String, String> getSkywarsSelectedKit() {
		return skywarsSelectedKit;
	}

	public void setSkywarsSelectedKit(HashMap<String, String> skywarsSelectedKit) {
		this.skywarsSelectedKit = skywarsSelectedKit;
	}

	public HashMap<String, Integer> getCachedCoins() {
		return coins;
	}
	public HashMap<String, Integer> getCachedLanguage() {
		return language;
	}

	public HashMap<String, String> getCachedSkyWarsKits() {
		return swKits;
	}

	public HashMap<String, Integer> getCachedSWCage() {
		return skywarsCage;
	}

	public HashMap<String, Integer> getCachedSkyWarsKills() {
		return skywarsKills;
	}

	public HashMap<String, Integer> getCachedSkyWarsDeaths() {
		return skywarsDeaths;
	}

	public HashMap<String, Integer> getCachedSkyWarsWins() {
		return skywarsWins;
	}

	public HashMap<String, Float> getCachedLevel() {
		return level;
	}

}
