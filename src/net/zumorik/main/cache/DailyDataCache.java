package net.zumorik.main.cache;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Locale;

import jdk.vm.ci.meta.Local;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import net.zumorik.main.Core;

public class DailyDataCache {

    private HashMap<String, String> user,vip,epic,elite,ultra, daily;

    private HashMap<String, Integer> first,second;

    public Core core;

    public DailyDataCache(Core core) {
        this.core = core;
        user = new HashMap<>();
        vip = new HashMap<>();
        epic = new HashMap<>();
        elite = new HashMap<>();
        ultra = new HashMap<>();
        daily = new HashMap<>();
        first = new HashMap<>();
        second = new HashMap<>();
    }


    public void setUser(Player p, String val){
        Bukkit.getScheduler().runTaskAsynchronously(core, ()->{
            core.getDailyDataCache().user.put(p.getName(), val);
            core.getDailyDatabase().setMonthlyUser(p, val);
        });
    }

    public void setVIP(Player p, String val){
        Bukkit.getScheduler().runTaskAsynchronously(core, ()->{
            core.getDailyDataCache().vip.put(p.getName(), val);
            core.getDailyDatabase().setMonthlyVIP(p, val);
        });
    }

    public void setEpic(Player p, String val){
        Bukkit.getScheduler().runTaskAsynchronously(core, ()->{
            core.getDailyDataCache().epic.put(p.getName(), val);
            core.getDailyDatabase().setMonthlyEpic(p, val);
        });
    }

    public void setElite(Player p, String val){
        Bukkit.getScheduler().runTaskAsynchronously(core, ()->{
            core.getDailyDataCache().elite.put(p.getName(), val);
            core.getDailyDatabase().setMonthlyElite(p, val);
        });
    }

    public void setUltra(Player p, String val){
        Bukkit.getScheduler().runTaskAsynchronously(core, ()->{
            core.getDailyDataCache().ultra.put(p.getName(), val);
            core.getDailyDatabase().setMonthlyUltra(p, val);
        });
    }

    public void setDaily(Player p, String val){
        Bukkit.getScheduler().runTaskAsynchronously(core, ()->{
            core.getDailyDataCache().daily.put(p.getName(), val);
            core.getDailyDatabase().setDaily(p, val);
        });
    }

    public void setFirstTime(Player p, int val){
        Bukkit.getScheduler().runTaskAsynchronously(core, ()->{
            core.getDailyDataCache().first.put(p.getName(), val);
            core.getDailyDatabase().setFirstTime(p, val);
        });
    }

    public void setSecondTime(Player p, int val){
        Bukkit.getScheduler().runTaskAsynchronously(core, ()->{
            core.getDailyDataCache().second.put(p.getName(), val);
            core.getDailyDatabase().setSecondTime(p, val);
        });
    }

    public LocalDateTime getMonthly(Player p){
        if(core.getDailyDataCache().user.containsKey(p.getName())) {
            String val = core.getDailyDataCache().user.get(p.getName());

            DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

            String vals = "2017-03-08 12:30";
            LocalDateTime time = LocalDateTime.parse(vals, format);

            if (!(val.equalsIgnoreCase("NA"))) {
                time = LocalDateTime.parse(val, format);
            }

            System.out.println(time);
            return time;
        }else{
            core.getDailyDataCache().user.put(p.getName(), core.getDailyDatabase().getMonthly(p, "monthly_user"));
            String val = core.getDailyDataCache().user.get(p.getName());
            String testTime = "2017-03-08 12:30";
            DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
            LocalDateTime time = LocalDateTime.parse(testTime, format);
            if (!(val.equalsIgnoreCase("NA"))) {
                time = LocalDateTime.parse(val, format);
            }
            return time;
        }
    }

    public LocalDateTime getMonthlyVIP(Player p){
        if(core.getDailyDataCache().vip.containsKey(p.getName())) {
            String val = core.getDailyDataCache().vip.get(p.getName());

            DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

            String vals = "2017-03-08 12:30";
            LocalDateTime time = LocalDateTime.parse(vals, format);

            if (!(val.equalsIgnoreCase("NA"))) {
                time = LocalDateTime.parse(val, format);
            }
            return time;
        }else{
            core.getDailyDataCache().vip.put(p.getName(), core.getDailyDatabase().getMonthly(p, "monthly_vip"));
            String val = core.getDailyDataCache().vip.get(p.getName());
            String testTime = "2017-03-08 12:30";
            DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
            LocalDateTime time = LocalDateTime.parse(testTime, format);
            if (!(val.equalsIgnoreCase("NA"))) {
                time = LocalDateTime.parse(val, format);
            }
            return time;
        }
    }

    public LocalDateTime getMonthlyEpic(Player p){
        if(core.getDailyDataCache().epic.containsKey(p.getName())) {
            String val = core.getDailyDataCache().epic.get(p.getName());

            DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

            String vals = "2017-03-08 12:30";
            LocalDateTime time = LocalDateTime.parse(vals, format);

            if (!(val.equalsIgnoreCase("NA"))) {
                time = LocalDateTime.parse(val, format);
            }
            return time;
        }else{
            core.getDailyDataCache().epic.put(p.getName(), core.getDailyDatabase().getMonthly(p, "monthly_epic"));
            String val = core.getDailyDataCache().epic.get(p.getName());
            String testTime = "2017-03-08 12:30";
            DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
            LocalDateTime time = LocalDateTime.parse(testTime, format);
            if (!(val.equalsIgnoreCase("NA"))) {
                time = LocalDateTime.parse(val, format);
            }
            return time;
        }
    }

    public LocalDateTime getMonthlyElite(Player p){
        if(core.getDailyDataCache().elite.containsKey(p.getName())) {
            String val = core.getDailyDataCache().elite.get(p.getName());

            DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

            String vals = "2017-03-08 12:30";
            LocalDateTime time = LocalDateTime.parse(vals, format);

            if (!(val.equalsIgnoreCase("NA"))) {
                time = LocalDateTime.parse(val, format);
            }
            return time;
        }else{
            core.getDailyDataCache().elite.put(p.getName(), core.getDailyDatabase().getMonthly(p, "monthly_elite"));
            String val = core.getDailyDataCache().elite.get(p.getName());
            String testTime = "2017-03-08 12:30";
            DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
            LocalDateTime time = LocalDateTime.parse(testTime, format);
            if (!(val.equalsIgnoreCase("NA"))) {
                time = LocalDateTime.parse(val, format);
            }
            return time;
        }
    }

    public LocalDateTime getMonthlyUltra(Player p){
        if(core.getDailyDataCache().ultra.containsKey(p.getName())) {
            String val = core.getDailyDataCache().ultra.get(p.getName());

            DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

            String vals = "2017-03-08 12:30";
            LocalDateTime time = LocalDateTime.parse(vals, format);

            if (!(val.equalsIgnoreCase("NA"))) {
                time = LocalDateTime.parse(val, format);
            }
            return time;
        }else{
            core.getDailyDataCache().ultra.put(p.getName(), core.getDailyDatabase().getMonthly(p, "monthly_ultra"));
            String val = core.getDailyDataCache().ultra.get(p.getName());
            String testTime = "2017-03-08 12:30";
            DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
            LocalDateTime time = LocalDateTime.parse(testTime, format);
            if (!(val.equalsIgnoreCase("NA"))) {
                time = LocalDateTime.parse(val, format);
            }
            return time;
        }
    }

    public LocalDateTime getDaily(Player p){
        if(core.getDailyDataCache().daily.containsKey(p.getName())) {
            String val = core.getDailyDataCache().daily.get(p.getName());

            DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

            String vals = "2017-03-08 12:30";
            LocalDateTime time = LocalDateTime.parse(vals, format);

            if (!(val.equalsIgnoreCase("NA"))) {
                time = LocalDateTime.parse(val, format);
            }
            return time;
        }else{
            core.getDailyDataCache().daily.put(p.getName(), core.getDailyDatabase().getMonthly(p, "daily"));
            String val = core.getDailyDataCache().daily.get(p.getName());
            String testTime = "2017-03-08 12:30";
            DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
            LocalDateTime time = LocalDateTime.parse(testTime, format);
            if (!(val.equalsIgnoreCase("NA"))) {
                time = LocalDateTime.parse(val, format);
            }
            return time;
        }
    }

    public int getFirstTime(Player p){
        if(core.getDailyDataCache().first.containsKey(p.getName())) {
            int val = core.getDailyDataCache().first.get(p.getName());
            return val;
        }else{
            core.getDailyDataCache().first.put(p.getName(), core.getDailyDatabase().getFirstTime(p));
            int val = core.getDailyDataCache().first.get(p.getName());
            return val;
        }
    }

    public int getSecondTime(Player p){
        if(core.getDailyDataCache().second.containsKey(p.getName())) {
            int val = core.getDailyDataCache().second.get(p.getName());
            return val;
        }else{
            core.getDailyDataCache().second.put(p.getName(), core.getDailyDatabase().getSecondTime(p));
            int val = core.getDailyDataCache().second.get(p.getName());
            return val;
        }
    }

    public void cacheUser(Player p) {
        core.getDailyDataCache().user.put(p.getName(), core.getDailyDatabase().getMonthly(p, "monthly_user"));
        core.getDailyDataCache().vip.put(p.getName(), core.getDailyDatabase().getMonthly(p, "monthly_vip"));
        core.getDailyDataCache().epic.put(p.getName(), core.getDailyDatabase().getMonthly(p, "monthly_epic"));
        core.getDailyDataCache().elite.put(p.getName(), core.getDailyDatabase().getMonthly(p, "monthly_elite"));
        core.getDailyDataCache().ultra.put(p.getName(), core.getDailyDatabase().getMonthly(p, "monthly_ultra"));
        core.getDailyDataCache().daily.put(p.getName(), core.getDailyDatabase().getMonthly(p, "daily"));
        core.getDailyDataCache().first.put(p.getName(), core.getDailyDatabase().getFirstTime(p));
        core.getDailyDataCache().second.put(p.getName(), core.getDailyDatabase().getSecondTime(p));
    }

    public void uncacheUser(Player p) {
        core.getDailyDataCache().user.remove(p.getName());
        core.getDailyDataCache().vip.remove(p.getName());
        core.getDailyDataCache().epic.remove(p.getName());
        core.getDailyDataCache().elite.remove(p.getName());
        core.getDailyDataCache().ultra.remove(p.getName());
        core.getDailyDataCache().daily.remove(p.getName());
        core.getDailyDataCache().first.remove(p.getName());
        core.getDailyDataCache().second.remove(p.getName());
    }

}
