package net.zumorik.main.commands;

import com.google.common.collect.Iterables;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import net.zumorik.bungeecord.pluginmessage.PunishmentSystem;
import net.zumorik.bungeecord.punish.BanReasons;
import net.zumorik.main.Core;
import net.zumorik.main.api.CoreAPI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class PunishmentCommands implements CommandExecutor {

    private Core core;

    public PunishmentCommands(Core core){
        this.core = core;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String args[]){

        if(cmd.getName().equalsIgnoreCase("unban")){
            if(sender instanceof Player){
                Player p = (Player) sender;

                if(core.getRankCache().hasModeratorPermissions(p)) {

                    if (args.length < 1) {
                        p.sendMessage(colo("&cCorrect usage: /unban <user>"));
                        return true;
                    }

                    String target = args[0];
                    Player t = Bukkit.getPlayer(target);

                    if (t != null) {
                        core.getStatsDatabase().setbanTime(t, LocalDate.now());
                        core.getStatsDatabase().setBanned(t, false);
                        core.getStatsDatabase().setBanReason(t, "null");
                    } else {
                        OfflinePlayer ts = Bukkit.getOfflinePlayer(target);
                        core.getStatsDatabase().setbanTime(ts, LocalDate.now());
                        core.getStatsDatabase().setBanned(ts, false);
                        core.getStatsDatabase().setBanReason(ts, "null");
                    }

                    p.sendMessage(colo("&b" + target + " &awas unbanned if they were banned."));
                    return true;
                }else{
                    p.sendMessage(colo("&cYou do not have permission to execute this command!"));
                }

            }else{
                if (args.length < 1) {
                    sender.sendMessage(colo("&cCorrect usage: /unban <user>"));
                    return true;
                }

                String target = args[0];
                Player t = Bukkit.getPlayer(target);

                if (t != null) {
                    core.getStatsDatabase().setbanTime(t, LocalDate.now());
                    core.getStatsDatabase().setBanned(t, false);
                    core.getStatsDatabase().setBanReason(t, "null");
                } else {
                    OfflinePlayer ts = Bukkit.getOfflinePlayer(target);
                    core.getStatsDatabase().setbanTime(ts, LocalDate.now());
                    core.getStatsDatabase().setBanned(ts, false);
                    core.getStatsDatabase().setBanReason(ts, "null");
                }

                sender.sendMessage(colo("&b" + target + " &awas unbanned if they were banned."));
                return true;
            }
        }

        if(cmd.getName().equalsIgnoreCase("ban")) {

            if (sender instanceof Player) {

                Player p = (Player) sender;

                if (core.getRankCache().hasModeratorPermissions(p)) {

                    if(args.length == 2){
                        String target = args[0];
                        String reason = args[1];
                        String time = "12/12/1000";
                        try {
                            BanReasons kick = BanReasons.valueOf(reason.toUpperCase());
                            sender.sendMessage(colo("&aRequest sent!"));
                            try {
                                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
                                LocalDate date = LocalDate.parse(time, formatter);

                                Player t = Bukkit.getPlayer(target);

                                if(t != null){
                                    core.getStatsDatabase().setbanTime(t, date);
                                    core.getStatsDatabase().setBanned(t, true);
                                    core.getStatsDatabase().setBanReason(t, kick.toString());
                                }else{
                                    OfflinePlayer ts = Bukkit.getOfflinePlayer(target);
                                    core.getStatsDatabase().setbanTime(ts, date);
                                    core.getStatsDatabase().setBanned(ts, true);
                                    core.getStatsDatabase().setBanReason(ts, kick.toString());
                                }

                                sendMessage("ban:" + target + ":" + kick.toString() + ":" + time);
                                for(Player ps : Bukkit.getOnlinePlayers()){
                                    if(core.getStatsCache().getLanguage(ps) == 1){
                                        ps.sendMessage(colo("&b" + target + " &ehas been banned from the network by &c&l" + sender.getName() +"."));
                                    }else{
                                        ps.sendMessage(colo("&b" + target + " &eha sido baneado de la red por &c&l " + sender.getName() +"."));
                                    }
                                }

                            } catch (Exception e) {
                                sender.sendMessage(colo("&cThat is not a valid time stamp correct way: dd/mm/yyyy example: 09/12/2020"));
                                return true;
                            }
                        } catch (IllegalArgumentException e) {
                            sender.sendMessage(colo("&cThat is not a valid ban reason!"));
                            return true;
                        }
                    }else if (args.length == 3){
                        String target = args[0];
                        String reason = args[1];
                        String time = args[2];

                        try {

                            BanReasons kick = BanReasons.valueOf(reason.toUpperCase());
                            sender.sendMessage(colo("&aRequest sent!"));
                            try {
                                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
                                LocalDate date = LocalDate.parse(time, formatter);

                                Player t = Bukkit.getPlayer(target);

                                if(t != null){
                                    core.getStatsDatabase().setbanTime(t, date);
                                    core.getStatsDatabase().setBanned(t, true);
                                    core.getStatsDatabase().setBanReason(t, kick.toString());
                                }else{
                                    OfflinePlayer ts = Bukkit.getOfflinePlayer(target);
                                    core.getStatsDatabase().setbanTime(ts, date);
                                    core.getStatsDatabase().setBanned(ts, true);
                                    core.getStatsDatabase().setBanReason(ts, kick.toString());
                                }
                                sendMessage("ban:" + target + ":" + kick.toString() + ":" + time);
                                for(Player ps : Bukkit.getOnlinePlayers()){
                                    if(core.getStatsCache().getLanguage(ps) == 1){
                                        ps.sendMessage(colo("&b" + target + " &ehas been banned from the network by &c&l" + sender.getName() +"."));
                                    }else{
                                        ps.sendMessage(colo("&b" + target + " &eha sido baneado de la red por &c&l" + sender.getName() +"."));
                                    }
                                }

                            } catch (Exception e) {
                                sender.sendMessage(colo("&cThat is not a valid time stamp correct way: dd/mm/yyyy example: 09/12/2020"));
                                e.printStackTrace();
                                return true;
                            }


                        } catch (IllegalArgumentException e) {
                            sender.sendMessage(colo("&cThat is not a valid ban reason!"));
                            return true;
                        }
                    }else{
                        p.sendMessage(colo("&cCorrect usage /ban user <banReason> <timeStamp> dd/mm/yyyy"));
                    }


                } else {
                    sender.sendMessage(colo("&cYou do not have permission to execute this command."));
                    return true;
                }

            }else{
                if(args.length == 2){
                    String target = args[0];
                    String reason = args[1];
                    String time = "12/12/1000";
                    try {
                        BanReasons kick = BanReasons.valueOf(reason.toUpperCase());
                        sender.sendMessage(colo("&aRequest sent!"));
                        try {
                            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
                            LocalDate date = LocalDate.parse(time, formatter);

                            Player t = Bukkit.getPlayer(target);

                            if(t != null){
                                core.getStatsDatabase().setbanTime(t, date);
                                core.getStatsDatabase().setBanned(t, true);
                                core.getStatsDatabase().setBanReason(t, kick.toString());
                            }else{
                                OfflinePlayer ts = Bukkit.getOfflinePlayer(target);
                                core.getStatsDatabase().setbanTime(ts, date);
                                core.getStatsDatabase().setBanned(ts, true);
                                core.getStatsDatabase().setBanReason(ts, kick.toString());
                            }
                            sendMessage("ban:" + target + ":" + kick.toString() + ":" + time);
                            for(Player ps : Bukkit.getOnlinePlayers()){
                                if(core.getStatsCache().getLanguage(ps) == 1){
                                    ps.sendMessage(colo("&b" + target + " &ehas been banned from the network by &c&lCONSOLE."));
                                }else{
                                    ps.sendMessage(colo("&b" + target + " &eha sido baneado de la red por &c&lCONSOLE."));
                                }
                            }
                        } catch (Exception e) {
                            sender.sendMessage(colo("&cThat is not a valid time stamp correct way: dd/mm/yyyy example: 09/12/2020"));
                            return true;
                        }
                    } catch (IllegalArgumentException e) {
                        sender.sendMessage(colo("&cThat is not a valid ban reason!"));
                        return true;
                    }
                }else if (args.length == 3){
                    String target = args[0];
                    String reason = args[1];
                    String time = args[2];

                    try {

                        BanReasons kick = BanReasons.valueOf(reason.toUpperCase());
                        sender.sendMessage(colo("&aRequest sent!"));
                        try {
                            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
                            LocalDate date = LocalDate.parse(time, formatter);

                            Player t = Bukkit.getPlayer(target);

                            if(t != null){
                                core.getStatsDatabase().setbanTime(t, date);
                                core.getStatsDatabase().setBanned(t, true);
                                core.getStatsDatabase().setBanReason(t, kick.toString());
                            }else{
                                OfflinePlayer ts = Bukkit.getOfflinePlayer(target);
                                core.getStatsDatabase().setbanTime(ts, date);
                                core.getStatsDatabase().setBanned(ts, true);
                                core.getStatsDatabase().setBanReason(ts, kick.toString());
                            }
                            sendMessage("ban:" + target + ":" + kick.toString() + ":" + time);
                            for(Player ps : Bukkit.getOnlinePlayers()){
                                if(core.getStatsCache().getLanguage(ps) == 1){
                                    ps.sendMessage(colo("&b" + target + " &ehas been banned from the network by &c&lCONSOLE."));
                                }else{
                                    ps.sendMessage(colo("&b" + target + " &eha sido baneado de la red por &c&lCONSOLE."));
                                }
                            }
                        } catch (Exception e) {
                            sender.sendMessage(colo("&cThat is not a valid time stamp correct way: dd/mm/yyyy example: 09/12/2020"));
                            return true;
                        }



                    } catch (IllegalArgumentException e) {
                        sender.sendMessage(colo("&cThat is not a valid ban reason!"));
                        return true;
                    }
                }else{
                    sender.sendMessage(colo("&cCorrect usage /ban user <banReason> <timeStamp> dd/mm/yyyy"));
                }
            }
        }

        if(cmd.getName().equalsIgnoreCase("kick")){

            if(!(sender instanceof Player)){

                    if(args.length == 0){
                        sender.sendMessage(colo("&cCorrect method: /kick <Player> <Reason>"));
                        return true;
                    }

                    String target = args[0];

                    if(args.length == 1){
                        sender.sendMessage(colo("&eSending kick request to proxy&8: &akicking " + target + "..."));
                        sendMessage("kick:" + target + ":DEFAULT");
                        sender.sendMessage(colo("&aRequest sent!"));


                        for(Player ps : Bukkit.getOnlinePlayers()){
                                if(core.getStatsCache().getLanguage(ps) == 1){
                                    ps.sendMessage(colo("&b" + target + " &ehas been kicked from the network by &c&lCONSOLE."));
                                }else{
                                    ps.sendMessage(colo("&b" + target + " &eha sido patiedo de la red por &c&lCONSOLE."));
                                }
                        }

                        return true;
                    }

                    String reason = args[1];

                    sender.sendMessage(colo("&eSending kick request to proxy&8: &akicking " + target + "..."));
                    try{
                        BanReasons kick = BanReasons.valueOf(reason.toUpperCase());
                        sendMessage("kick:" + target + ":" + kick.toString());
                        sender.sendMessage(colo("&aRequest sent!"));

                        for(Player ps : Bukkit.getOnlinePlayers()){
                                if(core.getStatsCache().getLanguage(ps) == 1){
                                    ps.sendMessage(colo("&b" + target + " &ehas been kicked from the network by &c&lCONSOLE."));
                                }else{
                                    ps.sendMessage(colo("&b" + target + " &eha sido patiedo de la red por &c&lCONSOLE."));
                                }
                        }

                    }catch(IllegalArgumentException e){
                        sender.sendMessage(colo("&cRequest failed! that is not a valid reason!"));
                    }

            }else{
                Player p = (Player) sender;
                Bukkit.getScheduler().runTaskAsynchronously(core, () -> {
                        if(core.getRankCache().hasModeratorPermissions(p)){
                            if(args.length == 0){
                                p.sendMessage(colo("&cCorrect method: /kick <Player> <Reason>"));
                                return;
                            }
                            String target = args[0];
                            if(args.length == 1){
                                p.sendMessage(colo("&eSending kick request to proxy&8: &akicking " + target + "..."));
                                sendMessage("kick:" + target + ":DEFAULT");
                                p.sendMessage(colo("&aRequest sent!"));
                                for (Player ps : Bukkit.getOnlinePlayers()) {
                                        if (core.getStatsCache().getLanguage(ps) == 1) {
                                            ps.sendMessage(colo("&b" + target + " &ehas been kicked from the network by &c&l" + core.getRankCache().getRank(p) + " &e" + p.getName()));
                                        } else {
                                            ps.sendMessage(colo("&b" + target + " &eha sido patiedo de la red por &c&l" + core.getRankCache().getRank(p) + " &e" + p.getName()));
                                        }
                                }
                                return;
                            }

                            String reason = args[1];
                            p.sendMessage(colo("&eSending kick request to proxy&8: &akicking " + target + "..."));
                            try{
                                BanReasons kick = BanReasons.valueOf(reason.toUpperCase());
                                sendMessage("kick:" + target + ":" + kick.toString());
                                p.sendMessage(colo("&aRequest sent!"));

                                for (Player ps : Bukkit.getOnlinePlayers()) {

                                        if (core.getStatsCache().getLanguage(ps) == 1) {
                                            ps.sendMessage(colo("&b" + target + " &ehas been kicked from the network by &c&l" + core.getRankCache().getRank(p) + " &e" + p.getName()));
                                        } else {
                                            ps.sendMessage(colo("&b" + target + " &eha sido patiedo de la red por &c&l" + core.getRankCache().getRank(p) + " &e" + p.getName()));
                                        }
                                }

                            }catch(IllegalArgumentException e){
                                p.sendMessage(colo("&cRequest failed! that is not a valid reason!"));
                            }


                        }else{
                            p.sendMessage(colo("&cYou do not have permission to execute this command."));
                            return;
                        }
                });
            }
        }

        return false;
    }


    public void sendMessage(String code){
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF(code);
        Player player = Iterables.getFirst(Bukkit.getOnlinePlayers(), null);
        player.sendPluginMessage(core, "Punish", out.toByteArray());
    }

    private String colo(String s){
        return ChatColor.translateAlternateColorCodes('&', s);
    }

}