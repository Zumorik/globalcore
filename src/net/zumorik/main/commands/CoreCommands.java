package net.zumorik.main.commands;

import net.zumorik.main.util.MessageUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.zumorik.main.Core;
import net.zumorik.main.rank.Rank;

public class CoreCommands implements CommandExecutor {

	private Core core;

	public CoreCommands(Core core) {
		this.core = core;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		if(cmd.getName().equalsIgnoreCase("globalcore")){
			Player p = (Player) sender;
			p.sendMessage(MessageUtil.prefix + colo(" &7| &aPrimary System database and Network controller."));
			p.sendMessage(MessageUtil.prefix + colo(" &7| &aControladora de systema y red pricipal."));
			p.sendMessage(MessageUtil.prefix + colo(" &7| &eDeveloped by: &cZumorik"));
			p.sendMessage(MessageUtil.prefix + colo(" &7| &eVersion: &b1.01-SNAPSHOT"));
		}
		if (cmd.getName().equalsIgnoreCase("cc")) {

			if (sender instanceof Player) {
				Player p = (Player) sender;
				Bukkit.getScheduler().runTaskAsynchronously(core, () -> {
					if (core.getRankCache().hasBasicSystemPermissions(p)) {
						for (int i = 0; i < 64; i++) {
							Bukkit.broadcastMessage(" ");
						}
					} else {
						p.sendMessage(colo("&aGlobalCore &8- &aYou do not have permission!"));
					}
					return;
				});
			}
		}
		if (cmd.getName().equalsIgnoreCase("fly")) {

			if (sender instanceof Player) {
					Player p = (Player) sender;
					if (core.getRankCache().hasFlyPermissions(p)) {
						if (core.getConfig().getBoolean("lobby") || core.getConfig().getBoolean("swlobby")) {
							if (p.isFlying()) {
								p.setFlying(false);
								p.setAllowFlight(false);
								p.sendMessage(MessageUtil.prefix + colo(" &8- &cFlight has been disabled."));
							} else {
								p.setAllowFlight(true);
								p.sendMessage(MessageUtil.prefix + colo(" &8- &aFlight has been enabled."));
								return true;
							}
						} else {
							p.sendMessage(MessageUtil.prefix + colo("& &8- &cCannot be used in game!"));
							p.sendMessage(MessageUtil.prefix + colo(" &8- &cNo se puede utilizar en juego!"));
							return false;
						}

					} else {
						p.sendMessage(MessageUtil.prefix + colo(" &8- &aYou do not have permission!"));
						return false;
					}
			}

		}
		return false;
	}

	private String colo(String s) {
		return ChatColor.translateAlternateColorCodes('&', s);
	}

}
