package net.zumorik.main.commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.zumorik.main.Core;
import net.zumorik.main.handlers.VanishHandler;

public class VanishCommand implements CommandExecutor {

	@SuppressWarnings("unused")
	private Core core;

	public VanishCommand(Core core) {
		this.core = core;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		if (cmd.getName().equalsIgnoreCase("vanish") || cmd.getName().equalsIgnoreCase("v")) {
			if (!(sender instanceof Player)) {
				return true;
			}
			Player p = (Player) sender;
			if (p.hasPermission("globalcore.vanish")) {

				if (VanishHandler.players.contains(p.getName())) {
					VanishHandler.players.remove(p.getName());
					p.sendMessage(colo("&f&lHiso&b&lCraft &8| &cVanish deactivated."));
					for (Player ps : Bukkit.getOnlinePlayers()) {
						ps.showPlayer(core, p);
					}
				} else {
					VanishHandler.players.add(p.getName());
					p.sendMessage(colo("&f&lHiso&b&lCraft &8| &aVanish acivated."));
					for (Player ps : Bukkit.getOnlinePlayers()) {
						ps.hidePlayer(core, p);
					}
				}
			} else {
				p.sendMessage(colo("&aGlobalCore &8- &aYou do not have permission!"));
				return true;
			}
		}
		return false;
	}

	private String colo(String s) {
		return ChatColor.translateAlternateColorCodes('&', s);
	}

}
