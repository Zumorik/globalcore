package net.zumorik.main.commands;

import net.zumorik.main.util.MessageUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.zumorik.main.Core;
import net.zumorik.main.handlers.FreezeHandler;

public class FreezeCommand implements CommandExecutor {

	private Core core;

	public FreezeCommand(Core core) {
		this.core = core;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {

		if (cmd.getName().equalsIgnoreCase("freeze")) {
			if (!(sender instanceof Player)) {
				return true;
			}
			Player p = (Player) sender;
			if (core.getRankCache().hasModeratorPermissions(p)) {

				if (args.length != 1) {
					p.sendMessage(colo("&aGlobalCore &8- &a/freeze <player>"));
					return true;
				}

				String target = args[0];
				Player t = Bukkit.getPlayer(target);
				FreezeHandler handler = new FreezeHandler(core);
				if (t != null) {
					if (handler.getFroozenPlayers().contains(target)) {
						handler.unfreezePlayer(t);
						p.sendMessage(MessageUtil.prefix + colo(" &8| &e" + target + " &ahas been unfrozen."));
						t.sendMessage(MessageUtil.prefix + colo(" &8| &aYou were unfrozen by &e" + p.getName()));
					} else {
						handler.freezePlayer(t);
						p.sendMessage(MessageUtil.prefix + colo(" &8| &e" + target + " &ahas been frozen"));
						t.sendMessage(MessageUtil.prefix + colo(" &8| &aYou have been frozen by &e" + p.getName()));
					}
				} else {
					p.sendMessage(colo("&aGlobalCore &8- &c" + target + " is not online!"));
					return true;
				}

			} else {
				p.sendMessage(colo("&aGlobalCore &8- &aYou do not have permission!"));
				return true;
			}
		}

		return false;
	}

	private String colo(String s) {
		return ChatColor.translateAlternateColorCodes('&', s);
	}

}
