package net.zumorik.main.commands.register;

import net.zumorik.main.Core;
import net.zumorik.main.commands.*;

public class RegisterCommands {

	private Core core;

	public RegisterCommands(Core core) {
		this.core = core;
	}

	public void registerCommands() {
		core.getCommand("addcoins").setExecutor(new EconomyCommands(core));
		core.getCommand("setcoins").setExecutor(new EconomyCommands(core));
		core.getCommand("resetcoins").setExecutor(new EconomyCommands(core));
		core.getCommand("vanish").setExecutor(new VanishCommand(core));
		core.getCommand("v").setExecutor(new VanishCommand(core));
		core.getCommand("gamemode").setExecutor(new GamemodeCommands());
		core.getCommand("freeze").setExecutor(new FreezeCommand(core));
		core.getCommand("cc").setExecutor(new CoreCommands(core));
		core.getCommand("fly").setExecutor(new CoreCommands(core));
		core.getCommand("globalcore").setExecutor(new CoreCommands(core));
		core.getCommand("kick").setExecutor(new PunishmentCommands(core));
		core.getCommand("ban").setExecutor(new PunishmentCommands(core));
		core.getCommand("unban").setExecutor(new PunishmentCommands(core));
	}

}
