package net.zumorik.main.commands;

import java.sql.SQLException;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.zumorik.main.Core;

public class EconomyCommands implements CommandExecutor {

	private Core core;

	public EconomyCommands(Core core) {
		this.core = core;
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		if (cmd.getName().equalsIgnoreCase("addcoins")) {
			if (!(sender instanceof Player)) {
				if (args.length != 2) {
					sender.sendMessage(colo("&aGlobalCore &8- &c/addcoins <player> <amount>"));
					return true;
				}
				if (isInteger(args[1])) {
					String target = args[0];
					if (Bukkit.getPlayer(target) != null) {

						Player t = Bukkit.getPlayer(target);
						int i = Integer.parseInt(args[1]);
						try {
							core.getStatsCache().addCoins(t, i);
							sender.sendMessage(colo("&aAdding &e" + i + " &agems to &b" + t.getName()));
							return true;
						} catch (Exception e) {
							sender.sendMessage(colo("&cAn Error has been encountered ensure that the database is online."));
							return true;
						}
					} else {
						OfflinePlayer t = Bukkit.getOfflinePlayer(target);
						int i = Integer.parseInt(args[1]);
						core.getStatsDatabase().addCoins(t.getUniqueId(), i);
						sender.sendMessage(colo("&aAdding &e" + i + " &agems to &b" + t.getName()));
						return true;
					}
				} else {
					sender.sendMessage(colo("&c" + args[1] + " that is not a number!"));
					return true;
				}
			}
			Player p = (Player) sender;
			if (p.hasPermission("globalcore.economy")) {
				if (args.length != 2) {
					p.sendMessage(colo("&aGlobalCore &8- &c/addcoins <player> <amount>"));
					return true;
				}
				if (isInteger(args[1])) {
					String target = args[0];
					if (Bukkit.getPlayer(target) != null) {

						Player t = Bukkit.getPlayer(target);
						int i = Integer.parseInt(args[1]);
						try {
							core.getStatsCache().addCoins(t, i);
							p.sendMessage(colo("&aAdding &e" + i + " &aGems to &b" + t.getName()));
							return true;
						} catch (Exception e) {
							p.sendMessage(colo("&cAn Error has been encountered ensure that the database is online."));
							return true;
						}
					} else {
						OfflinePlayer t = Bukkit.getOfflinePlayer(target);
						int i = Integer.parseInt(args[1]);
						core.getStatsDatabase().addCoins(t.getUniqueId(), i);
						p.sendMessage(colo("&aAdding &e" + i + " &agems to &b" + t.getName()));
						return true;
					}
				} else {
					p.sendMessage(colo("&c" + args[1] + " that is not a number!"));
					return true;
				}
			} else {
				p.sendMessage(colo("&aGlobalCore &8- &cYou do not have permission!"));
				return true;
			}
		}
		if (cmd.getName().equalsIgnoreCase("resetcoins")) {
			if (!(sender instanceof Player)) {
				if (args.length != 1) {
					sender.sendMessage(colo("&aGlobalCore &8- &c/resetcoins <player>"));
					return true;
				}
				String target = args[0];
				if (Bukkit.getPlayer(target) != null) {

					Player t = Bukkit.getPlayer(target);
					try {
						core.getStatsCache().removeCoins(t, core.getStatsCache().getCoins(t));
						sender.sendMessage(colo("&aResetting &b" + t.getName() + " &agems."));
						return true;
					} catch (Exception e) {
						sender.sendMessage(colo("&cAn Error has been encountered ensure that the database is online."));
						return true;
					}
				} else {
					OfflinePlayer t = Bukkit.getOfflinePlayer(target);
					core.getStatsDatabase().removeCoins(t.getUniqueId(),
							core.getStatsDatabase().getCoins(t.getUniqueId()));
					sender.sendMessage(colo("&aResetting &b" + t.getName() + " &agems."));
					return true;
				}
			}
			Player p = (Player) sender;
			if (p.hasPermission("globalcore.economy")) {
				if (args.length != 1) {
					p.sendMessage(colo("&aGlobalCore &8- &c/resetcoins <player>"));
					return true;
				}
				String target = args[0];
				if (Bukkit.getPlayer(target) != null) {

					Player t = Bukkit.getPlayer(target);
					try {
						core.getStatsCache().removeCoins(t, core.getStatsCache().getCoins(t));
						p.sendMessage(colo("&aResetting &b" + t.getName() + " &agems."));
						return true;
					} catch (Exception e) {
						p.sendMessage(colo("&cAn Error has been encountered ensure that the database is online."));
						return true;
					}
				} else {
					OfflinePlayer t = Bukkit.getOfflinePlayer(target);
					core.getStatsDatabase().removeCoins(t.getUniqueId(),
							core.getStatsDatabase().getCoins(t.getUniqueId()));
					p.sendMessage(colo("&aResetting &b" + t.getName() + " &agems."));
					return true;
				}
			} else {
				p.sendMessage(colo("&aGlobalCore &8- &aNo tienes permiso!"));
				return true;
			}
		}
		if (cmd.getName().equalsIgnoreCase("setcoins")) {
			if (!(sender instanceof Player)) {
				if (args.length != 2) {
					sender.sendMessage(colo("&aGlobalCore &8- &c/setcoins <player> <amount>"));
					return true;
				}
				if (isInteger(args[1])) {
					String target = args[0];
					if (Bukkit.getPlayer(target) != null) {

						Player t = Bukkit.getPlayer(target);
						int i = Integer.parseInt(args[1]);
						try {
							core.getStatsCache().removeCoins(t, core.getStatsCache().getCoins(t));
							core.getStatsCache().addCoins(t, i);
							sender.sendMessage(colo("&aAssigning &e" + i + " &agems to &b" + t.getName()));
							return true;
						} catch (Exception e) {
							sender.sendMessage(colo("&cAn Error has been encountered ensure that the database is online."));
							return true;
						}
					} else {
						OfflinePlayer t = Bukkit.getOfflinePlayer(target);
						int i = Integer.parseInt(args[1]);
						core.getStatsDatabase().removeCoins(t.getUniqueId(),
								core.getStatsDatabase().getCoins(t.getUniqueId()));
						core.getStatsDatabase().addCoins(t.getUniqueId(), i);
						sender.sendMessage(colo("&aAssigning &e" + i + " &agems to &b" + t.getName()));
						return true;
					}
				} else {
					sender.sendMessage(colo("&c" + args[1] + " is not a number!"));
					return true;
				}
			}
			Player p = (Player) sender;
			if (p.hasPermission("globalcore.economy")) {
				if (args.length != 2) {
					p.sendMessage(colo("&aGlobalCore &8- &c/addcoins <player> <amount>"));
					return true;
				}
				if (isInteger(args[1])) {
					String target = args[0];
					if (Bukkit.getPlayer(target) != null) {

						Player t = Bukkit.getPlayer(target);
						int i = Integer.parseInt(args[1]);
						try {
							core.getStatsCache().removeCoins(t, core.getStatsCache().getCoins(t));
							core.getStatsCache().addCoins(t, i);
							p.sendMessage(colo("&aAssigning &e" + i + " &agems to &b" + t.getName()));
							return true;
						} catch (Exception e) {
							p.sendMessage(colo("&cAn Error has been encountered ensure that the database is online."));
							return true;
						}
					} else {
						OfflinePlayer t = Bukkit.getOfflinePlayer(target);
						int i = Integer.parseInt(args[1]);
						core.getStatsDatabase().removeCoins(t.getUniqueId(),
								core.getStatsDatabase().getCoins(t.getUniqueId()));
						core.getStatsDatabase().addCoins(t.getUniqueId(), i);
						p.sendMessage(colo("&aAssigning &e" + i + " &agems to &b" + t.getName()));
						return true;
					}
				} else {
					p.sendMessage(colo("&c" + args[1] + " is not a number!"));
					return true;
				}
			} else {
				p.sendMessage(colo("&aGlobalCore &8- &aYou do not have permission!"));
				return true;
			}
		}
		return false;

	}

	private String colo(String s) {
		return ChatColor.translateAlternateColorCodes('&', s);
	}

	private boolean isInteger(String s) {
		try {
			Integer.parseInt(s);
		} catch (NumberFormatException | NullPointerException e) {
			return false;
		}
		return true;
	}

}
