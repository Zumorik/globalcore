package net.zumorik.main.commands;

import net.zumorik.main.util.MessageUtil;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;

public class GamemodeCommands implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		if (cmd.getName().equalsIgnoreCase("gamemode")) {
			if (!(sender instanceof Player)) {
				return true;
			}

			Player p = (Player) sender;
			if (p.hasPermission("globalcore.gamemode")) {
				if (args[0].equalsIgnoreCase("c")) {
					if (args.length == 2) {
						if (Bukkit.getPlayer(args[1]) != null) {
							Player target = Bukkit.getPlayer(args[1]);
							target.setGameMode(GameMode.CREATIVE);
							p.sendMessage(MessageUtil.prefix + colo(" &8| &eSetting " + target.getName()
									+ " gamemode to &7: &b&lCREATIVE"));
							return true;
						} else {
							p.sendMessage(MessageUtil.prefix + colo(" &8| &c" + args[1] + " is not online."));
							return true;
						}
					} else if (args.length == 1) {
						p.sendMessage(MessageUtil.prefix + colo(" &8| &eChanging gamemode to &7: &b&lCREATIVE"));
						p.setGameMode(GameMode.CREATIVE);
						return true;
					} else {
						return true;
					}
				}
				if (args[0].equalsIgnoreCase("s")) {
					if (args.length == 2) {
						if (Bukkit.getPlayer(args[1]) != null) {
							Player target = Bukkit.getPlayer(args[1]);
							target.setGameMode(GameMode.SURVIVAL);
							p.sendMessage(MessageUtil.prefix + colo(" &8| &eChanging " + target.getName()
									+ " gamemode to&7: &b&lSURVIVAL"));
							return true;
						} else {
							p.sendMessage(MessageUtil.prefix + colo(" &8| &c" + args[1] + " is not online!"));
							return true;
						}
					} else if (args.length == 1) {
						p.sendMessage(MessageUtil.prefix + colo(" &8| &eChanging gamemode to &7: &b&lSURVIVAL"));
						p.setGameMode(GameMode.SURVIVAL);
						return true;
					} else {
						return true;
					}
				}
			} else {
				p.sendMessage(colo("&aGlobalCore &8- &aYou do not have permission!"));
				return true;
			}
		}
		return false;
	}

	private String colo(String s) {
		return ChatColor.translateAlternateColorCodes('&', s);
	}

}
