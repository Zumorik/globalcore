package net.zumorik.main.handlers;

import java.util.List;

import org.bukkit.entity.Player;

import net.zumorik.main.Core;

public class FreezeHandler {

	private Core core;

	public FreezeHandler(Core core) {
		this.core = core;
	}

	public void freezePlayer(Player p) {
		List<String> players = core.getConfig().getStringList("Froozen");
		if (!players.contains(p.getName())) {
			players.add(p.getName());
			core.getConfig().set("Froozen", players);
			core.saveConfig();
			core.reloadConfig();
		}
	}

	public void unfreezePlayer(Player p) {
		List<String> players = core.getConfig().getStringList("Froozen");
		if (players.contains(p.getName())) {
			players.remove(p.getName());
			core.getConfig().set("Froozen", players);
			core.saveConfig();
			core.reloadConfig();
		}
	}

	public List<String> getFroozenPlayers() {
		return core.getConfig().getStringList("Froozen");
	}

}
