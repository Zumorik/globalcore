package net.zumorik.main.broadcaster;

import net.zumorik.main.Core;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.SQLException;

public class BroadcastTimer extends BukkitRunnable {

    private Core core;

    int time;

    public BroadcastTimer(Core core){
        this.core = core;
        time = core.getConfig().getInt("broadcast.time");
    }

    @Override
    public void run() {
        time--;
        if(time <= 0){
            time = core.getConfig().getInt("broadcast.time");
            try {
                core.getBroadcast().broadcastToServer();
            } catch (SQLException | ClassNotFoundException throwable) {
                throwable.printStackTrace();
            }

        }
    }
}
