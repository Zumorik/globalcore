package net.zumorik.main.broadcaster;

import net.zumorik.main.Core;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.sql.SQLException;
import java.util.List;

public class ServerBroadcast {

    private Core core;

    private int index;

    public ServerBroadcast(Core core){
        this.core = core;
        index = 1;
    }

    public List<String> getBroadcastMessage(Player p) throws SQLException, ClassNotFoundException {

        if(index > core.getConfig().getInt("broadcast.max")){
            index = 1;
        }

        List<String> spanish = core.getConfig().getStringList("broadcast." + index + "-s");

        List<String> english = core.getConfig().getStringList("broadcast." + index + "-e");

        if(core.getStatsCache().getLanguage(p) == 0)
            return spanish;
        else
            return english;
    }

    public void broadcastToServer() throws SQLException, ClassNotFoundException {
        for(Player p : Bukkit.getOnlinePlayers()){
            List<String> message = getBroadcastMessage(p);
            for(String s : message){
                p.sendMessage(colo(s));
            }
        }
        index++;
    }

    private String colo(String s){
        return ChatColor.translateAlternateColorCodes('&',s);
    }

}
