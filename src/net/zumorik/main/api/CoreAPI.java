package net.zumorik.main.api;

import net.zumorik.main.Core;
import net.zumorik.main.cache.MiscDataCache;
import net.zumorik.main.cache.RankCache;
import net.zumorik.main.cache.SkyWarsPerkCache;
import net.zumorik.main.cache.StatsCache;

public class CoreAPI {

	private static Core core;

	public CoreAPI(Core core) {
		CoreAPI.core = core;
	}

	public static Core getCore() {
		return core;
	}

	public static StatsCache getEconomy() {
		return core.getStatsCache();
	}

	public static MiscDataCache getMystery() {
		return core.getMysteryCache();
	}

	public static RankCache getRanks() {
		return core.getRankCache();
	}

	public static SkyWarsPerkCache getSkyWarsPerks() {
		return core.getSkywarsPerkCache();
	}

	public static StatsCache getStats() {
		return core.getStatsCache();
	}

}
