package net.zumorik.main;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import net.zumorik.main.broadcaster.BroadcastTimer;
import net.zumorik.main.broadcaster.ServerBroadcast;
import net.zumorik.main.cache.*;
import net.zumorik.main.mysql.*;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.common.collect.Iterables;

import net.md_5.bungee.api.ChatColor;
import net.zumorik.main.api.CoreAPI;
import net.zumorik.main.commands.register.RegisterCommands;
import net.zumorik.main.listeners.register.RegisterListeners;

public class Core extends JavaPlugin {

	private StatsDatabase statsDatabase;
	private StatsCache statsCache;
	private DailyDatabase dailyDatabase;
	private SkyWarsPerkDatabase skywarsPerkDatabase;
	private SkyWarsPerkCache skywarsPerkCache;
	private MiscDatabase mysteryBoxDatabase;
	private MiscDataCache miscDataCache;
	private RankDatabase rankDatabase;
	private RankCache rankCache;
	private DailyDataCache dailyDataCache;
	private PageStatsDatabase pageStatsDatabase;
	private RegisterListeners registerListeners;
	private RegisterCommands registerCommands;
	private ServerBroadcast broadcast;
	private CoreAPI coreAPI;

	@Override
	public void onEnable() {
		Bukkit.getConsoleSender().sendMessage(colo("&aGlobalCore &8- &eEnabling..."));
		Bukkit.getConsoleSender().sendMessage(colo("&aGlobalCore &8- &eLoading config files..."));
		saveDefaultConfig();
		Bukkit.getConsoleSender().sendMessage(colo("&aGlobalCore &8- &aComplete."));
		Bukkit.getConsoleSender().sendMessage(colo("&aGlobalCore &8- &eRegistering Listeners...."));
		setRegisterListeners(new RegisterListeners(this));
		setRegisterCommands(new RegisterCommands(this));
		getRegisterListeners().registerListeners();
		Bukkit.getConsoleSender().sendMessage(colo("&aGlobalCore &8- &aComplete."));
		Bukkit.getConsoleSender().sendMessage(colo("&aGlobalCore &8- &eLoading commands..."));
		getRegisterCommands().registerCommands();
		Bukkit.getConsoleSender().sendMessage(colo("&aGlobalCore &8- &aComplete."));
		Bukkit.getConsoleSender().sendMessage(colo("&aGlobalCore &8- &eLaunching Databases..."));
		setBroadcast(new ServerBroadcast(this));
		enableBroadcast();
		loadStaticDatabases();
		loadDatabases();
		Bukkit.getConsoleSender().sendMessage(colo("&aGlobalCore &8- &eOpening Network API..."));
		setCoreAPI(new CoreAPI(this));
		Bukkit.getConsoleSender().sendMessage(colo("&aGlobalCore &8- &aAPI Enabled and online."));
		this.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
		this.getServer().getMessenger().registerOutgoingPluginChannel(this, "Punish");
		Bukkit.getConsoleSender().sendMessage(colo("&aGlobalCore &8- &aDatabases online."));
		Bukkit.getConsoleSender().sendMessage(colo("&aGlobalCore &8- &aPlugin Enabled."));
	}

	@Override
	public void onDisable() {
		Bukkit.getConsoleSender().sendMessage(colo("&aGlobalCore &8- &cShutting down..."));
		Bukkit.getConsoleSender().sendMessage(colo("&aGlobalCore &8- &cClosing MySQL connections..."));
		unloadDatabases();
		Bukkit.getConsoleSender().sendMessage(colo("&aGlobalCore &8- &eSaving configs..."));
		Bukkit.getConsoleSender().sendMessage(colo("&aGlobalCore &8- &aConfigs saved!"));
		Bukkit.getConsoleSender().sendMessage(colo("&aGlobalCore &8- &cDisabled!"));
	}

	private void unloadDatabases() {
		try {
			this.getStatsDatabase().close();
			Bukkit.getConsoleSender().sendMessage(colo("&aGlobalCore &8- &cStats database shutdown."));
		} catch (Exception e) {

		}
		try {
			this.getMiscDatabase().close();
			Bukkit.getConsoleSender().sendMessage(colo("&aGlobalCore &8- &cMisc database shutdown."));
		} catch (Exception e) {

		}
		try {
			this.getRankDatabase().close();
			Bukkit.getConsoleSender().sendMessage(colo("&aGlobalCore &8- &cRank database shutdown."));
		} catch (Exception e) {

		}
		try {
			this.getPageStatsDatabase().close();
			Bukkit.getConsoleSender().sendMessage(colo("&aGlobalCore &8- &cPageStats database shutdown."));
		} catch (Exception e) {

		}
		try {
			this.getSkywarsPerkDatabase().close();
			Bukkit.getConsoleSender().sendMessage(colo("&aGlobalCore &8- &cSkywars Perks database shutdown."));
		} catch (Exception e) {

		}
		Bukkit.getConsoleSender().sendMessage(colo("&aGlobalCore &8- &aAll MySQL connections closed."));
	}

	private void loadStaticDatabases() {
		try{
		setStatsDatabase(new StatsDatabase(this));
		Bukkit.getConsoleSender().sendMessage(colo("&aGlobalCore &8- &aStats database enabled."));
		}catch(Exception e){
			Bukkit.getConsoleSender().sendMessage(colo("&aGlobalCore &8- &cStats database failed to load."));
		}
		setStatsCache(new StatsCache(this));
		Bukkit.getConsoleSender().sendMessage(colo("&aGlobalCore &8- &eStats cache enabled."));
		try{
		setRankDatabase(new RankDatabase(this));
		Bukkit.getConsoleSender().sendMessage(colo("&aGlobalCore &8- &aRank database enabled."));
		}catch(Exception e){
			Bukkit.getConsoleSender().sendMessage(colo("&aGlobalCore &8- &cRank database failed to load."));
		}
		setRankCache(new RankCache(this));
		Bukkit.getConsoleSender().sendMessage(colo("&aGlobalCore &8- &eRank cache enabled."));
	}

	private void loadDatabases() {
		if (getConfig().getBoolean("sw")) {
			try {
				setSkywarsPerkDatabase(new SkyWarsPerkDatabase(this));
				Bukkit.getConsoleSender().sendMessage(colo("&aGlobalCore &8- &aSW Perks database enabled."));
			}catch(Exception e){
				Bukkit.getConsoleSender().sendMessage(colo("&aGlobalCore &8- &cSW Perks database failed to load."));
			}
			setSkywarsPerkCache(new SkyWarsPerkCache(this));
			Bukkit.getConsoleSender().sendMessage(colo("&aGlobalCore &8- &eSW Perks cache enabled."));
			try{
			setMiscDataDatabase(new MiscDatabase(this));
			}catch(Exception e){
				Bukkit.getConsoleSender().sendMessage(colo("&aGlobalCore &8- &cMisc database failed to load."));
			}
			Bukkit.getConsoleSender().sendMessage(colo("&aGlobalCore &8- &aMisc database enabled."));
			setMiscDataCache(new MiscDataCache(this));
			Bukkit.getConsoleSender().sendMessage(colo("&aGlobalCore &8- &eMisc cache enabled."));
			try{
			setPageStatsDatabase(new PageStatsDatabase(this));
			Bukkit.getConsoleSender().sendMessage(colo("&aGlobalCore &8- &aPage states database enabled."));
			}catch(Exception e){
				Bukkit.getConsoleSender().sendMessage(colo("&aGlobalCore &8- &cPage stats database failed to load."));
			}

		}
		if (getConfig().getBoolean("swlobby")) {
			try{
			setSkywarsPerkDatabase(new SkyWarsPerkDatabase(this));
			Bukkit.getConsoleSender().sendMessage(colo("&aGlobalCore &8- &aSW Perks database enabled."));
			}catch(Exception e){
				Bukkit.getConsoleSender().sendMessage(colo("&aGlobalCore &8- &cSW Perks database failed to load."));
			}
			setSkywarsPerkCache(new SkyWarsPerkCache(this));
			Bukkit.getConsoleSender().sendMessage(colo("&aGlobalCore &8- &eSW Perks cache enabled."));
			try{
			setMiscDataDatabase(new MiscDatabase(this));
			Bukkit.getConsoleSender().sendMessage(colo("&aGlobalCore &8- &aMisc database enabled."));
			}catch(Exception e){
				Bukkit.getConsoleSender().sendMessage(colo("&aGlobalCore &8- &cMisc database failed to load."));
			}
			setMiscDataCache(new MiscDataCache(this));
			Bukkit.getConsoleSender().sendMessage(colo("&aGlobalCore &8- &eMisc cache enabled."));

			try{
				setDailyDatabase(new DailyDatabase(this));
				Bukkit.getConsoleSender().sendMessage(colo("&aGlobalCore &8- &aDaily database enabled."));
			}catch(Exception e){
				Bukkit.getConsoleSender().sendMessage(colo("&aGlobalCore &8- &cDaily database failed to load."));
			}
			setDailyDataCache(new DailyDataCache(this));
			Bukkit.getConsoleSender().sendMessage(colo("&aGlobalCore &8- &eDaily cache enabled."));
		}
		if (getConfig().getBoolean("lobby")) {
			try{
			setMiscDataDatabase(new MiscDatabase(this));
			Bukkit.getConsoleSender().sendMessage(colo("&aGlobalCore &8- &aMisc database enabled."));
			}catch(Exception e){
				Bukkit.getConsoleSender().sendMessage(colo("&aGlobalCore &8- &cMisc database failed to load."));
			}
			setMiscDataCache(new MiscDataCache(this));
			Bukkit.getConsoleSender().sendMessage(colo("&aGlobalCore &8- &eMisc cache enabled."));
			try{
				setDailyDatabase(new DailyDatabase(this));
				Bukkit.getConsoleSender().sendMessage(colo("&aGlobalCore &8- &aDaily database enabled."));
			}catch(Exception e){
				Bukkit.getConsoleSender().sendMessage(colo("&aGlobalCore &8- &cDaily database failed to load."));
			}
			setDailyDataCache(new DailyDataCache(this));
			Bukkit.getConsoleSender().sendMessage(colo("&aGlobalCore &8- &eDaily cache enabled."));
		}
		Bukkit.getConsoleSender().sendMessage(colo("&aGlobalCore &8- &aAll MySQL connections have attempted to open."));
	}

	public void enableBroadcast(){
		if(getConfig().getBoolean("lobby") || getConfig().getBoolean("swlobby")){
			new BroadcastTimer(this).runTaskTimer(this, 20,20);
		}
	}

	public StatsDatabase getStatsDatabase() {
		return statsDatabase;
	}

	public void setStatsDatabase(StatsDatabase statsDatabase) {
		this.statsDatabase = statsDatabase;
	}

	public StatsCache getStatsCache() {
		return statsCache;
	}

	public void setStatsCache(StatsCache statsCache) {
		this.statsCache = statsCache;
	}

	public SkyWarsPerkCache getSkywarsPerkCache() {
		return skywarsPerkCache;
	}

	public void setSkywarsPerkCache(SkyWarsPerkCache skywarsPerkCache) {
		this.skywarsPerkCache = skywarsPerkCache;
	}

	public SkyWarsPerkDatabase getSkywarsPerkDatabase() {
		return skywarsPerkDatabase;
	}

	public void setSkywarsPerkDatabase(SkyWarsPerkDatabase skywarsPerkDatabase) {
		this.skywarsPerkDatabase = skywarsPerkDatabase;
	}

	public void setBroadcast(ServerBroadcast broadcast){
		this.broadcast = broadcast;
	}

	public ServerBroadcast getBroadcast(){ return broadcast;}

	public MiscDatabase getMiscDatabase() {
		return mysteryBoxDatabase;
	}

	public DailyDatabase getDailyDatabase() {
		return dailyDatabase;
	}

	public void setDailyDatabase(DailyDatabase dailyDatabase) {
		this.dailyDatabase = dailyDatabase;
	}

	public void setMiscDataDatabase(MiscDatabase mysteryBoxDatabase) {
		this.mysteryBoxDatabase = mysteryBoxDatabase;
	}

	public void setDailyDataCache(DailyDataCache dailyDataCache){
		this.dailyDataCache = dailyDataCache;
	}

	public DailyDataCache getDailyDataCache() {
		return dailyDataCache;
	}

	public MiscDataCache getMysteryCache() {
		return miscDataCache;
	}

	public void setMiscDataCache(MiscDataCache mysteryCache) {
		this.miscDataCache = mysteryCache;
	}

	public RankDatabase getRankDatabase() {
		return rankDatabase;
	}

	public void setRankDatabase(RankDatabase rankDatabase) {
		this.rankDatabase = rankDatabase;
	}

	public RankCache getRankCache() {
		return rankCache;
	}

	public void setRankCache(RankCache rankCache) {
		this.rankCache = rankCache;
	}

	public PageStatsDatabase getPageStatsDatabase() {
		return pageStatsDatabase;
	}

	public void setPageStatsDatabase(PageStatsDatabase pageStatsDatabase) {
		this.pageStatsDatabase = pageStatsDatabase;
	}

	private String colo(String s) {
		return ChatColor.translateAlternateColorCodes('&', s);
	}

	public RegisterListeners getRegisterListeners() {
		return registerListeners;
	}

	public void setRegisterListeners(RegisterListeners registerListeners) {
		this.registerListeners = registerListeners;
	}

	public RegisterCommands getRegisterCommands() {
		return registerCommands;
	}

	public void setRegisterCommands(RegisterCommands registerCommands) {
		this.registerCommands = registerCommands;
	}

	public CoreAPI getCoreAPI() {
		return coreAPI;
	}

	public void setCoreAPI(CoreAPI coreAPI) {
		this.coreAPI = coreAPI;
	}

	public void updateEntireNetwork() {
		ByteArrayOutputStream b = new ByteArrayOutputStream();
		DataOutputStream out = new DataOutputStream(b);
		try {
			out.writeUTF("network");
			out.writeUTF("Notch");
		} catch (IOException e) {

		}
		Player player = Iterables.getFirst(Bukkit.getOnlinePlayers(), null);
		player.sendPluginMessage(this, "BungeeCord", b.toByteArray());
	}

}
