package net.zumorik.bungeecord.pluginmessage;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Map;

import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.event.PluginMessageEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import net.zumorik.bungeecord.BungeeCore;

public class CoinMultiplier implements Listener {

	private BungeeCore core;

	public CoinMultiplier(BungeeCore core) {
		this.core = core;
	}

	@EventHandler
	public void onPluginMessage(PluginMessageEvent e) {
		if (e.getTag().equalsIgnoreCase("BungeeCord")) {
			DataInputStream in = new DataInputStream(new ByteArrayInputStream(e.getData()));
			try {
				String channel = in.readUTF(); // channel we delivered
				if (channel.equals("network") || channel.equals("Coins")) {
					sendDataBack("BungeeCord", "update");
				}
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}

	public void sendDataBack(String channel, String message) {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		DataOutputStream out = new DataOutputStream(stream);
		try {
			out.writeUTF(channel);
			out.writeUTF(message);
		} catch (IOException e) {
			e.printStackTrace();
		}
		Map<String, ServerInfo> map = core.getProxy().getServers();
		for (Map.Entry<String, ServerInfo> entry : map.entrySet()) {
			entry.getValue().sendData("BungeeCord", stream.toByteArray());
		}
	}

}
