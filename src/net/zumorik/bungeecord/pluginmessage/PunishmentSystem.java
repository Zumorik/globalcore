package net.zumorik.bungeecord.pluginmessage;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Map;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PluginMessageEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.event.EventHandler;
import net.zumorik.bungeecord.BungeeCore;
import net.zumorik.bungeecord.punish.BanReasons;

public class PunishmentSystem implements Listener {

    private BungeeCore core;

    public PunishmentSystem(BungeeCore core) {
        this.core = core;
    }

    @EventHandler
    public void onPluginMessage(PluginMessageEvent e) {
        if (e.getTag().equalsIgnoreCase("Punish")) {
            DataInputStream in = new DataInputStream(new ByteArrayInputStream(e.getData()));
            try {
                String channel =  in.readUTF();
                if (channel.contains("kick")) {
                   String[] split =  channel.split(":");
                    ProxiedPlayer p = core.getProxy().getPlayer(split[1]);
                    if(p != null) {
                        String reason = split[2];
                        if (reason.equalsIgnoreCase("default")) {
                            kick(p, BanReasons.DEFAULT);
                        } else {
                            BanReasons ban = BanReasons.valueOf(reason);
                            kick(p, ban);
                        }
                    }else{
                        System.out.println("An attempt to kick " + split[1] + " was executed but they are not online!");
                    }
                }else if (channel.contains("ban")){
                    String[] split = channel.split(":");

                    ProxiedPlayer p = core.getProxy().getPlayer(split[1]);
                    String reason = split[2];
                    String time = split[3];

                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
                    LocalDate date = LocalDate.parse(time,formatter);

                    String timed = date.toString();

                    kickBan(p, BanReasons.valueOf(reason), timed);

                }
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }

    public void kick(ProxiedPlayer p, BanReasons ban){
        Configuration r = core.getConfigs().getMSG();
        String msg = r.getString("punish." + ban.toString().toUpperCase());
        p.disconnect(sendMSG(msg));
    }

    public void kickBan(ProxiedPlayer p, BanReasons ban, String time){
        Configuration r = core.getConfigs().getMSG();
        String msg = r.getString("punish.ban." + ban.toString().toUpperCase()).replaceAll("%time%", time);
        p.disconnect(sendMSG(msg));
    }

    public BaseComponent sendMSG(String s) {
        return (BaseComponent) new TextComponent(ChatColor.translateAlternateColorCodes('&', s));
    }

    public void sendDataBack(String channel, String message) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(stream);
        try {
            out.writeUTF(channel);
            out.writeUTF(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Map<String, ServerInfo> map = core.getProxy().getServers();
        for (Map.Entry<String, ServerInfo> entry : map.entrySet()) {
            entry.getValue().sendData("BungeeCord", stream.toByteArray());
        }
    }

}
