package net.zumorik.bungeecord.party;

import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.zumorik.bungeecord.BungeeCore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PartyHandler {

    private HashMap<ProxiedPlayer, Party> parties;

    private HashMap<ProxiedPlayer, List<ProxiedPlayer>> invites;

    private BungeeCore core;

    public PartyHandler(BungeeCore core){
        this.core = core;
        parties = new HashMap<>();
        invites = new HashMap<>();
    }

    public HashMap<ProxiedPlayer, Party> getParties(){
        return parties;
    }

    public HashMap<ProxiedPlayer, List<ProxiedPlayer>> getInvites() {
        return invites;
    }

    public void addInvite(ProxiedPlayer invitor, ProxiedPlayer p){
        List<ProxiedPlayer> invites;
        if(core.getPartyHandler().getInvites().containsKey(p)) {
            invites = core.getPartyHandler().getInvites().get(p);
        }else{
            invites = new ArrayList<ProxiedPlayer>();
        }
        invites.add(invitor);
        core.getPartyHandler().getInvites().put(p, invites);
    }

    public void createParty(ProxiedPlayer p, ProxiedPlayer inv){
        Party party = new Party(p, inv, core);
        core.getPartyHandler().getParties().put(p,party);
        core.getPartyHandler().getParties().put(inv,party);
    }

    public Party getParty(ProxiedPlayer p){
        Party ps = null;
        if(core.getPartyHandler().getParties().containsKey(p)){
            ps = core.getPartyHandler().getParties().get(p);
        }
        if(ps == null){
            for(Map.Entry<ProxiedPlayer, Party> entry: parties.entrySet()){
                ProxiedPlayer leader = entry.getKey();
                Party z = entry.getValue();
                if(z.getMembers().contains(p)){
                    ps = z;
                    break;
                }
            }
        }
        return ps;
    }



}
