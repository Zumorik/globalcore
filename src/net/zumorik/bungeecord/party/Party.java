package net.zumorik.bungeecord.party;

import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.zumorik.bungeecord.BungeeCore;

import java.util.ArrayList;
import java.util.List;

public class Party {


    private ProxiedPlayer leader;
    private List<ProxiedPlayer> members;

    private BungeeCore core;


    public Party(ProxiedPlayer leader, List<ProxiedPlayer> members, BungeeCore core){
        this.core = core;
        this.leader = leader;
        this.members = members;

    }

    public Party(ProxiedPlayer leader, ProxiedPlayer invited, BungeeCore core){
        this.core = core;
        this.leader = leader;
        members = new ArrayList<ProxiedPlayer>();
    }

    public Party(ProxiedPlayer leader, BungeeCore core){
        this.core = core;
        this.leader = leader;
        members = new ArrayList<ProxiedPlayer>();
    }

    public void setMembers(List<ProxiedPlayer> members){
        this.members = members;
    }

    public void changeLeader(ProxiedPlayer p){
        this.leader = p;
    }

    public ProxiedPlayer getLeader(){
        return leader;
    }

    public void setLeader(ProxiedPlayer p){
        this.leader = p;
    }

    public void addPartyMember(ProxiedPlayer p){
        if(!members.contains(p))
        members.add(p);
    }

    public List<ProxiedPlayer> getMembers(){
        return members;
    }

    public void removePartyMember(ProxiedPlayer p){
        if(members.contains(p))
        members.remove(p);
    }

    public boolean isLeader(ProxiedPlayer p){
        if(core.getPartyHandler().getParty(p).getLeader() == p)
            return true;
        else
            return false;
    }

    public void disbandParty(){
        core.getPartyHandler().getParties().remove(leader);
    }

}
