package net.zumorik.bungeecord.commands;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.connection.Server;
import net.md_5.bungee.api.plugin.Command;
import net.zumorik.bungeecord.BungeeCore;
import net.zumorik.bungeecord.party.Party;

import java.lang.reflect.Member;
import java.util.List;

public class PartyCommands extends Command {

    private BungeeCore core;

    private final String PREFIX = "&e&lHiso&9&lParty &7: ";
    private final String TITLE = "&e&lHiso&9&lParty";

    public PartyCommands(BungeeCore core) {
        super("party" , "party.use" , "p");
        this.core = core;
    }

    @Override
    public void execute(CommandSender sender, String[] args) {

        if(sender instanceof ProxiedPlayer){

            ProxiedPlayer p = (ProxiedPlayer) sender;

            if(args.length == 0){
                p.sendMessage(colo("      " + TITLE + "      "));
                p.sendMessage(colo( " "));
                p.sendMessage(colo( "&e/party invite <player"));
                p.sendMessage(colo( "&7Invite player to your party."));
                p.sendMessage(colo( " "));
                p.sendMessage(colo( "&e/party promote <player"));
                p.sendMessage(colo( "&7Set a player as the party leader"));
                p.sendMessage(colo( " "));
                p.sendMessage(colo( "&e/party warp"));
                p.sendMessage(colo( "&7Teleport party to your server."));
                p.sendMessage(colo( " "));
                p.sendMessage(colo( "&e/party disband"));
                p.sendMessage(colo( "&7Disband your party."));
                p.sendMessage(colo( " "));
                p.sendMessage(colo( "&e/party leave"));
                p.sendMessage(colo( "&7Leave the party."));
                p.sendMessage(colo( " "));
                p.sendMessage(colo( "&e/party remove <player>"));
                p.sendMessage(colo( "&7Remove a player from the party."));
                p.sendMessage(colo( " "));
                p.sendMessage(colo( "&e/party accept <name>"));
                p.sendMessage(colo( "&7Accept party invitation"));
                p.sendMessage(colo( " "));
                p.sendMessage(colo( "&e/party chat "));
                p.sendMessage(colo( "&7Join or leave party chat"));
                p.sendMessage(colo( " "));
                p.sendMessage(colo("      " + TITLE + "      "));
                return;
            }

            if(args[0].equalsIgnoreCase("invite")){
                if(args.length < 2){
                    p.sendMessage(colo(PREFIX + "&c/party invite <member>"));
                    return;
                }
                String target = args[1];
                if(core.getProxy().getPlayer(target) != null){
                    ProxiedPlayer t = core.getProxy().getPlayer(target);
                    if(core.getPartyHandler().getParty(p) != null){
                        if(core.getPartyHandler().getParty(p).isLeader(p)){

                            if(p == t){
                                p.sendMessage(colo(PREFIX + "&cYou cannot invite yourself to a party!"));
                                return;
                            }

                            if(core.getPartyHandler().getParty(p).getMembers().contains(t)){
                                p.sendMessage(colo(PREFIX + "&c" + t.getName() + " is already in your party!"));
                                return;
                            }

                            if(core.getPartyHandler().getInvites().containsKey(t)) {
                                List<ProxiedPlayer> inv = core.getPartyHandler().getInvites().get(t);
                                if (inv.contains(p)) {
                                    p.sendMessage(colo(PREFIX + "&cYou have already sent an invite to " + t.getName()));
                                    return;
                                }else{
                                    core.getPartyHandler().addInvite(p, t);
                                    p.sendMessage(colo(PREFIX + "&eInvite has been sent to &b" + t.getName()));
                                    t.sendMessage(colo(PREFIX + "&b" + p.getName() + " &ehas invited you to their party!"));
                                    t.sendMessage(colo(PREFIX + "&e" + "/party accept &b" + p.getName()));
                                    System.out.println("1");
                                }
                            }else{
                                core.getPartyHandler().addInvite(p, t);
                                p.sendMessage(colo(PREFIX + "&eInvite has been sent to &b" + t.getName()));
                                t.sendMessage(colo(PREFIX + "&b" + p.getName() + " &ehas invited you to their party!"));
                                t.sendMessage(colo(PREFIX + "&e" + "/party accept &b" + p.getName()));
                                System.out.println("11");
                            }
                        }else{
                            p.sendMessage(colo(PREFIX + "&cYou are not the party leader!"));
                            return;
                        }
                    }else{
                        Party party = new Party(p, core);
                        System.out.println("2");
                        core.getPartyHandler().addInvite(p,t);
                        core.getPartyHandler().getParties().put(p, party);
                        p.sendMessage(colo(PREFIX + "&eInvite has been sent to &b" + t.getName()));
                        t.sendMessage(colo(PREFIX + "&b" + p.getName() + " &ehas invited you to their party!"));
                        t.sendMessage(colo(PREFIX + "&e" + "/party accept &b" + p.getName()));
                        return;
                    }
                }else{
                    p.sendMessage(colo(PREFIX + "&cThat player is not online!"));
                    return;
                }
            }

            if(args[0].equalsIgnoreCase("accept")){
                if(args.length < 2){
                    p.sendMessage(colo(PREFIX + "&c/party accept <player>"));
                    return;
                }
                String target = args[1];
                if(core.getProxy().getPlayer(target) != null){
                    ProxiedPlayer t = core.getProxy().getPlayer(target);

                    if(core.getPartyHandler().getInvites().containsKey(p)){
                        List<ProxiedPlayer> invites = core.getPartyHandler().getInvites().get(p);
                        if(invites.contains(t)){
                                if(core.getPartyHandler().getParty(p) == null){
                                    invites.remove(t);
                                    core.getPartyHandler().getInvites().put(p, invites);
                                    core.getPartyHandler().getParty(t).addPartyMember(p);
                                    p.sendMessage(colo(PREFIX + "&eYou have accepted &b" + t.getName() + " &eparty request."));
                                    t.sendMessage(colo(PREFIX + "&b" + p.getName() + " &6has accepted your party request!"));

                                    for(ProxiedPlayer pz : core.getPartyHandler().getParty(p).getMembers()){
                                        pz.sendMessage(colo(PREFIX + "&b" + p.getName() + " &6has joined the party!"));
                                    }

                                }else{
                                    p.sendMessage(colo(PREFIX + "&cYou must leave your current party!"));
                                    return;
                                }
                        }else if(core.getPartyHandler().getParty(p) != null){
                            p.sendMessage(colo(PREFIX + "&cYou must leave your current party!"));
                            return;
                        }else{
                            p.sendMessage(colo(PREFIX + "&c" + t.getName() + " has not invited you to a party!"));
                            return;
                        }
                    }else{
                        p.sendMessage(colo(PREFIX + "&cYou do not have any invites!"));
                        return;
                    }
                }else{
                    p.sendMessage(colo(PREFIX + "&cThat player is not online!"));
                    return;
                }
            }

            if(args[0].equalsIgnoreCase("list")){

                if(core.getPartyHandler().getParty(p) != null){

                    p.sendMessage(colo("      " + TITLE + "      "));
                    p.sendMessage(colo(" "));
                    p.sendMessage(colo("&eLeader&7&l: &a" + core.getPartyHandler().getParty(p).getLeader()));
                    p.sendMessage(colo(" "));
                    p.sendMessage(colo("&eMembers&7: "));
                    for(ProxiedPlayer ps : core.getPartyHandler().getParty(p).getMembers()){
                        p.sendMessage(colo( "&a- &b" + ps.getName()));
                    }
                    p.sendMessage(colo(" "));
                    p.sendMessage(colo("      " + TITLE + "      "));
                    return;
                }else{
                    p.sendMessage(colo(PREFIX + "&cYou are not in a party!"));
                    return;
                }
            }

            if(args[0].equalsIgnoreCase("disband")){
                if(core.getPartyHandler().getParty(p) != null){
                    if(core.getPartyHandler().getParty(p).isLeader(p)){
                        List<ProxiedPlayer> members = core.getPartyHandler().getParty(p).getMembers();
                        p.sendMessage(colo(PREFIX + "&cYou have disbanded the party!"));
                        core.getPartyHandler().getParties().remove(p);
                        for(ProxiedPlayer ps : members){
                            ps.sendMessage(colo(PREFIX + "&c" + p.getName() + " has disbanded the party!"));
                        }
                    }else{
                        p.sendMessage(colo(PREFIX + "&cYou are not the party leader."));
                        return;
                    }
                }else{
                    p.sendMessage(colo(PREFIX + "&cYou are not in a party."));
                    return;
                }
            }

            if(args[0].equalsIgnoreCase("leave")){
                if(core.getPartyHandler().getParty(p) != null){
                    if(core.getPartyHandler().getParty(p).isLeader(p)){
                        p.sendMessage(colo(PREFIX + "&cYou are the party leader you cannot leave!"));
                        p.sendMessage(colo(PREFIX + "&cChange leadership or disband the party."));
                        return;
                    }else{
                        Party t = core.getPartyHandler().getParty(p);
                        List<ProxiedPlayer> members = core.getPartyHandler().getParty(p).getMembers();
                        members.remove(p);
                        t.setMembers(members);
                        for(ProxiedPlayer pz : members){
                            pz.sendMessage(colo(PREFIX + "&b" + p.getName() + " &chas left the party."));
                        }
                        p.sendMessage(colo(PREFIX + "&cYou have left the party."));
                        return;
                    }
                }else{
                    p.sendMessage(colo(PREFIX + "&cYou are not in a party."));
                    return;
                }
            }

            if(args[0].equalsIgnoreCase("warp")){
                if(core.getPartyHandler().getParty(p) != null){
                    if(core.getPartyHandler().getParty(p).isLeader(p)){

                        List<ProxiedPlayer> members = core.getPartyHandler().getParty(p).getMembers();

                        ServerInfo target = p.getServer().getInfo();

                        for(ProxiedPlayer ps : members){
                            ps.sendMessage(colo(PREFIX + "&b" + p.getName() + " &eis teleporting you to server&7: &9" + target.getName() + "&e..."));
                            ps.connect(target);
                        }
                        p.sendMessage(colo(PREFIX + "&eWarping all party members to server&7: &9" + target.getName()));
                        return;
                    }else{
                        p.sendMessage(colo(PREFIX + "&cYou are not the party leader!"));
                        return;
                    }
                }else{
                    p.sendMessage(colo(PREFIX + "&cYou are not in a party."));
                    return;
                }
            }

            if(args[0].equalsIgnoreCase("remove")){
                if(args.length < 2){
                    p.sendMessage(colo(PREFIX + "&c/party remove <player>"));
                    return;
                }
                if(core.getPartyHandler().getParty(p) != null){
                    if(core.getPartyHandler().getParty(p).isLeader(p)){

                        String target = args[1];

                        if(core.getProxy().getPlayer(target) != null) {

                            ProxiedPlayer t = core.getProxy().getPlayer(target);

                            if(core.getPartyHandler().getParty(p).getMembers().contains(t)) {

                                List<ProxiedPlayer> members = core.getPartyHandler().getParty(p).getMembers();
                                members.remove(t);
                                core.getPartyHandler().getParty(p).setMembers(members);
                                t.sendMessage(colo(PREFIX + "&c" + p.getName() + " has kicked you from the party."));
                                p.sendMessage(colo(PREFIX + "&c" + t.getName() + " has been kicked from the party."));
                                for (ProxiedPlayer pz : members) {
                                    pz.sendMessage(colo(PREFIX + "&b" + p.getName() + " &chas been kicked from the party."));
                                }
                                return;
                            }else {
                                p.sendMessage(colo(PREFIX + "&c" + target + " is not in your party!"));
                            }
                        }else{
                            p.sendMessage(colo(PREFIX + "&c" + target + " is not online!"));
                            return;
                        }
                    }else{
                        p.sendMessage(colo(PREFIX + "&cYou are not the party leader!"));
                    }
                }else{
                    p.sendMessage(colo(PREFIX + "&cYou are not in a party."));
                    return;
                }
            }


            if(args[0].equalsIgnoreCase("promote")){
                if(args.length < 2){
                    p.sendMessage(colo(PREFIX + "&c/party promote <player>"));
                    return;
                }

                String target = args[1];

                if(core.getProxy().getPlayer(target) != null) {

                    if (core.getPartyHandler().getParty(p) != null) {

                        if (core.getPartyHandler().getParty(p).isLeader(p)) {

                            ProxiedPlayer t = core.getProxy().getPlayer(target);

                            if(core.getPartyHandler().getParty(p).getMembers().contains(t)){

                                ProxiedPlayer oldLeader = core.getPartyHandler().getParty(p).getLeader();
                                ProxiedPlayer newLeader = t;

                                List<ProxiedPlayer> members = core.getPartyHandler().getParty(p).getMembers();
                                members.add(oldLeader);
                                members.remove(newLeader);
                                core.getPartyHandler().getParties().remove(oldLeader);

                                Party party  = new Party(newLeader, members, core);

                                oldLeader.sendMessage(colo(PREFIX + "&cYou are no longer the party leader!"));
                                for(ProxiedPlayer pz : members){
                                    pz.sendMessage(colo(PREFIX + "&6" + newLeader.getName() + " has been promoted to leader!"));
                                }
                                newLeader.sendMessage(colo(PREFIX + "&b" + oldLeader.getName() + " &6has promoted you to the party leader."));
                                core.getPartyHandler().getParties().put(newLeader,party);
                                return;
                            }else if (core.getPartyHandler().getParty(p).getLeader() == p){
                                p.sendMessage(colo(PREFIX + "&cYou already are the party leader!"));
                                p.sendMessage(colo(PREFIX + "&cOr " + target + " is not in your party!"));
                                return;
                            }else{
                                p.sendMessage(colo(PREFIX + "&c" + target + " is not in your party!"));
                            }


                        } else {
                            p.sendMessage(colo(PREFIX + "&cYou are not the party leader!"));
                        }

                    } else {
                        p.sendMessage(colo(PREFIX + "&cYou are not in a party!"));
                        return;
                    }
                }else{
                    p.sendMessage(colo(PREFIX + "&c" +target + " is not online!"));
                    return;
                }
            }



        }else{
            sender.sendMessage(colo("&cYou must be a player to run this command!"));
            return;
        }


    }


    public BaseComponent colo(String s) {
        return (BaseComponent) new TextComponent(ChatColor.translateAlternateColorCodes('&', s));
    }
}
