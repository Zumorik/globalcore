package net.zumorik.bungeecord;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;
import net.zumorik.bungeecord.commands.PartyCommands;
import net.zumorik.bungeecord.party.PartyHandler;
import net.zumorik.bungeecord.pluginmessage.CoinMultiplier;
import net.zumorik.bungeecord.pluginmessage.PunishmentSystem;
import net.zumorik.bungeecord.punish.Configs;

public class BungeeCore extends Plugin {

	private Configs configs;
	private PartyHandler partyHandler;

	@Override
	public void onEnable() {
		setConfigs(new Configs(this));
		getConfigs().createMessages();
		ProxyServer.getInstance().getPluginManager().registerListener(this, new CoinMultiplier(this));
		ProxyServer.getInstance().getPluginManager().registerListener(this, new PunishmentSystem(this));
		ProxyServer.getInstance().registerChannel("BungeeCord");
		ProxyServer.getInstance().registerChannel("Punish");
		setPartyHandler(new PartyHandler(this));
		getProxy().getPluginManager().registerCommand(this, new PartyCommands(this));

	}

	@Override
	public void onDisable() {

	}

	public PartyHandler getPartyHandler() {
		return partyHandler;
	}

	public void setPartyHandler(PartyHandler partyHandler) {
		this.partyHandler = partyHandler;
	}

	public Configs getConfigs() {
		return configs;
	}

	public void setConfigs(Configs configs) {
		this.configs = configs;
	}
}
