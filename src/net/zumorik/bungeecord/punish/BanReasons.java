package net.zumorik.bungeecord.punish;

public enum BanReasons {

    CHEATING,
    INAPPROPRIATE,
    ADVERTISING,
    CROSS_TEAMING,
    TEAM_KILLING,
    HACKING,
    SHARING_ACCOUNT,
    DISRESPECT,
    THREAT,
    EXPLOIT,
    MODDING,
    DEFAULT,
    CHEAT,
    FLY

}
