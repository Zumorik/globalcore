package net.zumorik.bungeecord.punish;

import com.google.common.io.ByteStreams;

import java.io.*;

import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;
import net.zumorik.bungeecord.BungeeCore;

public class Configs {


    private BungeeCore plugin;
    private File file;

    public Configs(BungeeCore core) {
        this.plugin = core;
    }

    public void createMessages() {
        if (!plugin.getDataFolder().exists()) {
            plugin.getDataFolder().mkdir();
        }
        File configFile = new File(plugin.getDataFolder(), "messages.yml");
        if (!configFile.exists()) {
            try {
                configFile.createNewFile();
                try (InputStream is = plugin.getResourceAsStream("messages.yml");
                     OutputStream os = new FileOutputStream(configFile)) {

                    System.out.println(os);
                    System.out.println(is);

                     ByteStreams.copy(is, os);
                }
            } catch (IOException e) {
                throw new RuntimeException("Unable to create configuration file", e);
            }
        }
    }


    public Configuration getMSG() {
        File file = new File(plugin.getDataFolder(), "messages.yml");
        Configuration configuration = null;
        try {
            configuration = ConfigurationProvider.getProvider(YamlConfiguration.class).load(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return configuration;
    }
}
